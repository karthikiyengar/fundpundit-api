import mailgun from 'mailgun-js';
import moment from 'moment';
import cloudinary from 'cloudinary';
import unoconv from 'unoconv2';
import fs from 'fs';
import tmp from 'tmp';
import path from 'path';
import Docxtemplater from 'docxtemplater';
import Bluebird from 'bluebird';
import JSZip from 'jszip';
import { emailKeys, siteUrls, userTypes } from './constants';

const mailgunObject = new mailgun({
  apiKey: emailKeys.APIKEY,
  domain: emailKeys.DOMAINNAME
});

const createTmpFile = Bluebird.promisify(tmp.file, { multiArgs: true });
const appendFile = Bluebird.promisify(fs.appendFile);
const convert = Bluebird.promisify(unoconv.convert);

const generateLink = (token, user, type) => `${siteUrls.URL}/verify-user?token=${token}&user=${user}&type=${type}`;
const generateUserProfileLink = userId => `${siteUrls.SITECITYADMINURL}/customer/${userId}`;

export async function uploadPdfAndGetUrl(kyr, user, { grade, meaning, recommendation }) {
  const content = fs.readFileSync(path.join(__dirname, './templates', 'KYR.docx'));
  const zip = new JSZip(content);
  const doc = new Docxtemplater().loadZip(zip);

  // set the templateVariables
  doc.setData({
    fullName: `${user.firstName} ${user.lastName}`,
    email: user.email,
    phoneNumber: user.phoneNumber,
    subproduct: kyr.subproduct,
    sector: kyr.sector,
    stage: kyr.stage,
    amount: kyr.amount,
    responses: kyr.responses,
    createdAt: moment(kyr.createdAt).format('Do MMMM, YYYY'),
    grade,
    [`grade-${grade.toLowerCase()}`]: true,
    meaning,
    recommendation
  });

  doc.render();
  const docxResult = doc.getZip()
    .generate({ type: 'nodebuffer' });

  const [docxPath, docxFd, docxCleanup] = await createTmpFile({ prefix: 'gql-', postfix: '.docx' }); // eslint-disable-line
  await appendFile(docxPath, docxResult);
  const pdfResult = await convert(docxPath, 'pdf', {});
  const [pdfPath, pdfFd, pdfCleanup] = await createTmpFile(({ prefix: 'gql-', postfix: '.pdf' })); // eslint-disable-line
  await appendFile(pdfPath, pdfResult);
  const uploaded = await cloudinary.v2.uploader.upload(pdfPath);
  pdfCleanup();
  docxCleanup();
  return Promise.resolve(uploaded);
}


export function sendForgotPasswordMail(toMail, user, verificationToken) {
  const link = `${siteUrls.URL}/forgot-password?token=${verificationToken}&user=${user._id}&type=user`;
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Reset Password',
    html: `
      <span>
        Hey ${user.firstName},
      </span>
      <br />
      <span>
        There has been a request to reset your password. Please click <a href='${link}'>here</a> to proceed.
      </span>
      <br />
      <span>
        Alternatively, you can copy and paste this link into your browser:
      </span>
      <span>${link}</span>
      <br />
      <span>If you are unsure as to why you are receiving this email, you can safely ignore this.</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendVerificationEmail(toMail, firstName, lastName, user, verificationToken) {
  const link = generateLink(verificationToken, user, userTypes.USER);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Email Verification',
    html: `
      <span>
        Hey ${firstName},
      </span>
      <br />
      <span>
        This is a verification email from Fundpundit. Please click <a href='${link}'>here</a> to authorize your account.
      </span>

      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}


export function sendBankAdminVerificationEmail(toMail, name, bank, verificationToken) {
  const link = generateLink(verificationToken, bank, userTypes.BANK_ADMIN);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Email Verification',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        This is a verification email from Fundpundit. Please click <a href='${link}'>here</a> to authorize your account.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendCityAdminVerificationEmail(toMail, name, bank, verificationToken) {
  const link = generateLink(verificationToken, bank, userTypes.CITY_ADMIN);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Email Verification',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        This is a verification email from Fundpundit. Please click <a href='${link}'>here</a> to authorize your account.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendSiteCityAdminVerificationEmail(toMail, name, bank, verificationToken) {
  const link = generateLink(verificationToken, bank, userTypes.SITE_CITY_ADMIN);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Email Verification',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        This is a verification email from Fundpundit. Please click <a href='${link}'>here</a> to authorize your account.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendTestimonalEmail(toMail, name, branchId, userId) {
  const link = `${siteUrls.URL}/testimonial?branch=${branchId}&user=${userId}`;
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: Testimonial Request',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        This email is regarding you experience with Fundpundit. We would love to get your feedback on a couple of questions, please click <a href='${link}'>here</a> to leave your testimonial.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function emailKyrStatus(toMail, name) {
  const link = `${siteUrls.URL}`;
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: KYR Application progress',
    html: `
    <body style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:18px; color:rgba(102,102,102,1.00)">
      <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" style="border:2px solid maroon;">
        <tbody>
          <tr>
            <td align="center"><a href="http://fundpundit.com/" target="_blank"><img src="http://webgrowinfotech.com/fundpundit-emailer/fund-pundit-emailer-header.jpg" width="800" height="308" alt=""/></a></td>
          </tr>
          <tr>
            <td height="237"><table width="85%" border="0" align="center">
              <tbody>
                <tr>
                  <td height="33"><p>Dear ${name},</p></td>
                </tr>
                <tr>
                  <td height="98">There has been an update in one of your applications. 
                    <a href="${link}">Please click here</a> to login and see the progress.
      Or copy and paste this link into your browser: ${link}</td>
                </tr>
                <tr>
                  <td height="23">&nbsp;</td>
                </tr>
              </tbody>
            </table></td>
          </tr>
          <tr>
            <td align="center"><a href="http://fundpundit.com/" target="_blank"><img src="http://webgrowinfotech.com/fundpundit-emailer/fund-pundit-emailer-footer.jpg" width="800" height="169" alt=""/></a></td>
          </tr>
          <tr>
            <td><table width="100%" border="0">
              <tbody>
                <tr>
                  <td width="50%"><a href="https://www.facebook.com/fundpunditcom-758609570896401/?ref=hl" target="_blank"><img src="http://webgrowinfotech.com/fundpundit-emailer/facebook-icon-fund-pundit.png" width="34" height="34" alt=""/></a> <a href="https://twitter.com/fundpundit" target="_blank"><img src="http://webgrowinfotech.com/fundpundit-emailer/twitter-icon-fund-pundit.png" width="34" height="34" alt=""/></a> <a href="https://www.linkedin.com/company/fundpundit" target="_blank"><img src="http://webgrowinfotech.com/fundpundit-emailer/linkedin-icon-fund-pundit.jpg" width="34" height="34" alt=""/></a></td>
                  <td width="50%" align="right">© <a href="http://fundpundit.com/">Fund Pundit</a>.</td>
                </tr>
              </tbody>
            </table></td>
          </tr>
        </tbody>
      </table>
    </body>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendNewKyrNoticeEmail(toMail, name) {
  const link = `${siteUrls.CITYADMINURL}`;
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: New KYR Application',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        We are very glad to inform you that your branch has recevied new KYR application. Please click <a href='${link}'>here</a> to login and see the all KYRs.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendOddBusinessAppication(toMail, siteAdminName, fullName, city, email, phoneNumber, business, amount) {
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: New Business Focused Funding Application',
    html: `
      <span>
        Hey ${siteAdminName},
      </span>
      <br />
      <span>
        We are very glad to inform you that you have recevied new Business focused funding application. Below are details of user.
      </span>
      <br />
      <span>Name: ${fullName}</span><br/>
      <span>City: ${city}</span><br/>
      <span>email: ${email}</span><br/>
      <span>Contact No: ${phoneNumber}</span><br/>
      <span>Business: ${business}</span><br/>
      <span>Amount: ${amount}</span><br/>

      <span>
        Thank you.
      </span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendNewKyrNoticeEmailCityAdmin(toMail, name, userId) {
  const link = generateUserProfileLink(userId);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: New KYR Application',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        We are very glad to inform you that your city has recevied new KYR application. Please click <a href='${link}'>here</a> to login and see KYR of this user.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendKyrRejectNoticeEmailCityAdmin(toMail, name, userId) {
  const link = generateUserProfileLink(userId);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: KYR Application Rejection',
    html: `
      <span>
        Hey ${name},
      </span>
      <br />
      <span>
        A KYR application is reject by bank please take actions. Please click <a href='${link}'>here</a> to login and see the rejected KYR of this user.
      </span>
      <br />
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function escalateMailToAdmin(toMail, adminName, userFullName, bankName, userEmail, userPhoneNumber, subproduct, amount, userId) {
  const link = generateUserProfileLink(userId);
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: KYR Application Escalate Mail',
    html: `
      <span>
        Hey ${adminName},
      </span>
      <br />
      <span>
        We are want to inform you that KYR request of below given user is not progressing. Please click <a href='${link}'>here</a> to login and see KYR of this user.
      </span>
      <br />
      <br />
      <span>Name: ${userFullName}</span><br/>
      <span>Bank: ${bankName}</span><br/>
      <span>email: ${userEmail}</span><br/>
      <span>Phone Number: ${userPhoneNumber}</span><br/>
      <span>Subproduct: ${subproduct}</span><br/>
      <span>Amount: ${amount}</span><br/>
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
    `
  };
  return mailgunObject.messages().send(data);
}

export function sendNewPopUserNoticeEmail(toMail, adminName, userFullName, userEmail, userPhoneNumber, subproduct, amount, city) {
  const link = 'http://fundpundit.com/site-admin/customer/unregistered';
  const data = {
    from: emailKeys.FROM,
    to: toMail,
    subject: 'Fundpundit: New User',
    html: `
      <span>
        Hey ${adminName},
      </span>
      <br />
      <span>
        We are very glad to inform you that you have recevied new user search. Below are details of user. Please click <a href='${link}'>here</a> to login and see user.
      </span>
      <br />
      <span>Name: ${userFullName}</span><br/>
      <span>City: ${city}</span><br/>
      <span>email: ${userEmail}</span><br/>
      <span>Contact No: ${userPhoneNumber}</span><br/>
      <span>Subproduct: ${subproduct}</span><br/>
      <span>Amount: ${amount}</span><br/>
      <span>
        Or copy and paste this link into your browser:
      </span>
      <span>${link}</span>
      <span>
        Thank you.
      </span>
    `
  };
  return mailgunObject.messages().send(data);
}
