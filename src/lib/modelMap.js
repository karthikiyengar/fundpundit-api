import SiteAdmin from '../graphql/types/site-admin';
import SiteCityAdmin from '../graphql/types/site-city-admin';
import BankAdmin from '../graphql/types/bank-admin';
import CityAdmin from '../graphql/types/city-admin';
import User from '../graphql/types/user';
import { userTypes } from './constants';

export default {
  [userTypes.SITE_ADMIN]: SiteAdmin,
  [userTypes.USER]: User,
  [userTypes.BANK_ADMIN]: BankAdmin,
  [userTypes.CITY_ADMIN]: CityAdmin,
  [userTypes.SITE_CITY_ADMIN]: SiteCityAdmin
};
