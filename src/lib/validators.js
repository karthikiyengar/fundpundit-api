//TODO Get rid of this as soon as possible

// @flow
import validate from 'mongoose-validator';
import logger from './logger';


export function createError(message: string, param: any = null, value : any = null) : Array {
    return {
      errors: [{
        param,
        value,
        message
      }]
    };
}

export function formatMongooseError(err : Object) {
    const error = err.errors;
    if (err.name !== 'ValidationError') {
      return createError('Internal Server Error');
    }
    const params = Object.keys(error);
    return {
      errors: params.map(item => ({
        param: item,
        message: error[item].properties.message,
        value: error[item].properties.value
      }))
    };
}


export const nameValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 25],
    message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters'
  }),
    validate({
    validator: 'matches',
    arguments: ['^[a-zA-Z0-9\\-\\s]+$'],
    message: 'Name should contain alpha-numeric characters only'
  })
];

export const contactNumberValidator = [
  validate({
    validator: 'isLength',
    arguments: [10, 13],
    message: 'Invalid contact number'
  })
];

export const emailValidator = [
  validate({
    validator: 'isEmail'
  })
];

/**
 * This function encapsulates error handling for all operations
 * 1. Mongoose Validation errors
 * 2. Other errors are handled using with a generic error
 */
export async function executeSafe(fn: any) {
  try {
    const result = await fn();
    return Promise.resolve({ result });
  } catch (err) {
    if (err.name === 'ValidationError') {
      return Promise.resolve(formatMongooseError(err));
    } else {
      return Promise.resolve(createError('Internal Server Error'));
    }
  }
}
