export const userTypes = {
  SITE_ADMIN: 'site-admin',
  USER: 'user',
  BANK_ADMIN: 'bank-admin',
  CITY_ADMIN: 'city-admin',
  SITE_CITY_ADMIN: 'site-city-admin'
};
export const productTypes = {
  CORPORATE: 'corporate',
  PESONAL: 'personal',
  INVESTMENT: 'investment'
};
export const emailKeys = {
  APIKEY: 'key-1b1cefdf780514ca200edd4335c92e9f',
  DOMAINNAME: 'mg.fundpundit.com',
  FROM: 'support@fundpundit.com'
};
export const siteUrls = {
  URL: 'https://fundpundit.com',
  SITECITYADMINURL: 'https://fundpundit.com/site-city-admin',
  CITYADMINURL: 'https://fundpundit.com/city-admin'
};
