import Joi from 'joi';
import * as constants from './constants';

Joi.objectId = require('joi-objectid')(Joi);

export default {
  login: Joi.object().keys({
    type: Joi.valid(Object.values(constants.userTypes)),
    email: Joi.string().email(),
    password: Joi.string().min(3).max(30)
  }),
  addUser: {
    firstName: Joi.string().min(3).max(30),
    lastName: Joi.string().min(3).max(30),
    email: Joi.string().email(),
    password: Joi.string().min(3).max(30)
  },
  registerUser: {
    firstName: Joi.string().min(3).max(30),
    lastName: Joi.string().min(3).max(30),
    email: Joi.string().email(),
    phoneNumber: Joi.string(),
    password: Joi.string().min(3).max(30)
  },
  submitKyr: Joi.object().keys({
    amountId: Joi.objectId(),
    subproductId: Joi.objectId(),
    branchId: Joi.objectId(),
    sectorId: Joi.objectId().optional(),
    stageId: Joi.objectId().optional(),
    responses: Joi.array().items(Joi.object().keys({
      questionId: Joi.objectId(),
      optionId: Joi.objectId()
    })),
    otherBranches: Joi.array().optional(),
    user: Joi.object().keys({
      firstName: Joi.string(),
      lastName: Joi.string(),
      email: Joi.string().email(),
      phoneNumber: Joi.string()
    })
  }),
  addBankAdmin: Joi.object().keys({
    firstName: Joi.string().min(3).max(30),
    lastName: Joi.string().min(3).max(30),
    email: Joi.string().email(),
    bankId: Joi.objectId()
  }),
  addBranch: Joi.object().keys({
    address: Joi.string(),
    location: Joi.string(),
    branchEmail: Joi.string().optional(),
    bankId: Joi.objectId(),
    cityId: Joi.objectId(),
    latitude: Joi.string(),
    longitude: Joi.string(),
    contactNumber: Joi.string(),
    email: Joi.string().email()
  }),
  addProduct: Joi.object().keys({
    name: Joi.string().min(3).max(30),
    label: Joi.string().min(3).max(30)
  }),
  addBank: Joi.object().keys({
    name: Joi.string(),
    email: Joi.string().email(),
    contactNumber: Joi.string(),
    address: Joi.string(),
    description: Joi.string(),
    logo: Joi.string(),
    subproduct: Joi.objectId(),
    details: Joi.array(),
    criteria: Joi.object()
  }),
  addCityAdmin: Joi.object().keys({
    firstName: Joi.string().min(3).max(30),
    lastName: Joi.string().min(3).max(30),
    email: Joi.string().email(),
    cityId: Joi.objectId(),
    bankId: Joi.objectId()
  }),
  addSiteCityAdmin: Joi.object().keys({
    firstName: Joi.string().min(3).max(30),
    lastName: Joi.string().min(3).max(30),
    email: Joi.string().email(),
    cityId: Joi.objectId()
  }),
  addSubproduct: Joi.object().keys({
    label: Joi.string().min(3).max(30),
    productId: Joi.objectId()
  }),
  addService: Joi.object().keys({
    label: Joi.string(),
    image: Joi.string()
  }),
  addCompany: Joi.object().keys({
    companyName: Joi.string(),
    contactName: Joi.string(),
    companyLogo: Joi.string().optional(),
    contactNumber: Joi.string(),
    address: Joi.string(),
    description: Joi.string(),
    serviceOffered: Joi.string(),
    serviceId: Joi.objectId()
  }),
  addTeam: Joi.object().keys({
    name: Joi.string(),
    image: Joi.string(),
    description: Joi.string()
  }),
  addTestimonial: Joi.object().keys({
    branchId: Joi.objectId(),
    userId: Joi.objectId(),
    review: Joi.string(),
    rating: Joi.string()
  }),
  addOddBusiness: Joi.object().keys({
    label: Joi.string(),
    image: Joi.string()
  }),
  addOddBusinessApplicant: Joi.object().keys({
    fullName: Joi.string(),
    email: Joi.string(),
    amount: Joi.string(),
    phoneNumber: Joi.string(),
    comment: Joi.string().optional(),
    oddBusinessId: Joi.objectId(),
    city: Joi.objectId().optional()
  }),
  addPopupUser: Joi.object().keys({
    fullName: Joi.string(),
    email: Joi.string(),
    phoneNumber: Joi.string(),
    subproductId: Joi.objectId(),
    stageId: Joi.objectId().optional(),
    sectorId: Joi.objectId().optional(),
    amountId: Joi.objectId(),
    cityId: Joi.objectId().optional()
  }),
  addFundGyan: Joi.object().keys({
    label: Joi.string(),
    image: Joi.string()
  }),
  addYoutubeLink: Joi.object().keys({
    link: Joi.string(),
    title: Joi.string().optional(),
    description: Joi.string().optional()
  }),
  addOffer: Joi.object().keys({
    title: Joi.string(),
    description: Joi.string()
  }),
  updateBankAdmin: Joi.object().keys({
    id: Joi.objectId(),
    firstName: Joi.string().min(3).max(30).optional(),
    lastName: Joi.string().min(3).max(30).optional(),
    email: Joi.string().email().optional(),
    bankId: Joi.objectId().optional()
  }),
  updateBranch: Joi.object().keys({
    id: Joi.objectId(),
    address: Joi.string().optional(),
    location: Joi.string().optional(),
    branchEmail: Joi.string().optional(),
    bankId: Joi.objectId().optional(),
    cityId: Joi.objectId().optional(),
    latitude: Joi.string().optional(),
    longitude: Joi.string().optional(),
    contactNumber: Joi.string().optional(),
    email: Joi.string().email().optional()
  }),
  updateProduct: Joi.object().keys({
    id: Joi.objectId(),
    name: Joi.string().min(3).max(30).optional(),
    label: Joi.string().min(3).max(30).optional()
  }),
  updateBank: Joi.object().keys({
    id: Joi.objectId(),
    name: Joi.string().optional(),
    email: Joi.string().email().optional(),
    contactNumber: Joi.string().optional(),
    address: Joi.string().optional(),
    description: Joi.string().optional(),
    logo: Joi.string().optional(),
    subproducts: Joi.object().keys({ id: Joi.array().items(Joi.objectId()) }).optional(),
    details: Joi.array(),
    criteria: Joi.object()
  }),
  updateCityAdmin: Joi.object().keys({
    id: Joi.objectId(),
    firstName: Joi.string().min(3).max(30).optional(),
    lastName: Joi.string().min(3).max(30).optional(),
  }),
  updateSiteCityAdmin: Joi.object().keys({
    id: Joi.objectId(),
    firstName: Joi.string().min(3).max(30).optional(),
    lastName: Joi.string().min(3).max(30).optional(),
  }),
  updateSubproduct: Joi.object().keys({
    id: Joi.objectId(),
    label: Joi.string().min(3).max(30).optional(),
    productId: Joi.objectId().optional()
  }),
  updateService: Joi.object().keys({
    id: Joi.objectId(),
    label: Joi.string().optional(),
    image: Joi.string().optional()
  }),
  updateCompany: Joi.object().keys({
    id: Joi.objectId(),
    companyName: Joi.string().optional(),
    contactName: Joi.string().optional(),
    companyLogo: Joi.string().optional(),
    contactNumber: Joi.string().optional(),
    address: Joi.string().optional(),
    description: Joi.string().optional(),
    serviceOffered: Joi.string().optional(),
    serviceId: Joi.objectId().optional()
  }),
  updateTeam: Joi.object().keys({
    id: Joi.objectId(),
    name: Joi.string().optional(),
    image: Joi.string().optional().optional(),
    description: Joi.string().optional()
  }),
  updateTestimonial: Joi.object().keys({
    id: Joi.objectId(),
    review: Joi.string().optional(),
    isVerified: Joi.boolean().optional()
  }),
  updateOddBusiness: Joi.object().keys({
    id: Joi.objectId(),
    label: Joi.string().optional(),
    image: Joi.string().optional()
  }),
  updateUser: Joi.object().keys({
    firstName: Joi.string().min(3).max(30).optional(),
    lastName: Joi.string().min(3).max(30).optional(),
    profilePic: Joi.string().optional()
  }),
  updatePassword: Joi.object().keys({
    currentPassword: Joi.string().min(3).max(30),
    password: Joi.string().min(3).max(30),
    confirmPassword: Joi.string().min(3).max(30),
  }),
  addQuestion: Joi.object().keys({
    subproductId: Joi.objectId(),
    question: Joi.string(),
    hasCalculator: Joi.boolean().optional(),
    weight: Joi.string(),
    options: Joi.array().items(Joi.object().keys({
      label: Joi.string(),
      value: Joi.string()
    }))
  }),
  deleteQuestion: Joi.object().keys({
    id: Joi.objectId()
  }),
  updateFundGyan: Joi.object().keys({
    id: Joi.objectId(),
    label: Joi.string().optional(),
    image: Joi.string().optional()
  }),
  updateYoutubeLink: Joi.object().keys({
    id: Joi.objectId(),
    link: Joi.string().optional(),
    title: Joi.string().optional(),
    description: Joi.string().optional()
  }),
  updateOffer: Joi.object().keys({
    id: Joi.objectId(),
    title: Joi.string().optional(),
    description: Joi.string().optional()
  }),
  allKyrsForCity: Joi.object().keys({
    cityId: Joi.objectId().optional(),
    bankId: Joi.objectId().optional(),
    filter: Joi.valid(['all', 'underProcess', 'approved', 'rejected', 'new']).optional(),
  })
};
