// @flow

import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import modelMap from './modelMap';


export function generateSalt(len: number = 10) {
  return crypto
    .randomBytes(Math.ceil(len / 2))
    .toString('hex')
    .substring(0, len);
}

export function sha512(password: string, salt: string): string {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return value;
}

function generateToken(type: string, user: Object) {
  return jwt.sign({ type }, process.env.JWT_PRIVATE_KEY, {
    expiresIn: '60d',
    algorithm: 'RS256',
    subject: user._id.toString()
  });
}

export async function authenticateAndGenerateToken(type: string, email: string, password: string) {
  const user = await modelMap[type].findOne({ email });
  if (!user) {
    throw Error('User does not exist');
  }
  if (!user.isVerified) {
    throw Error('User is not verified');
  }
  if (user.password === sha512(password, user.salt)) {
    const token = generateToken(type, user);
    return ({
      ...user.toJSON(),
      token
    });
  }
  throw Error('Invalid Credentials');
}

export async function verifyTokenAndGetUser(token: String) {
  try {
    const decoded = jwt.verify(token, process.env.JWT_PUBLIC_KEY);
    const { type, sub } = decoded;
    const user = await modelMap[type].findById(sub);
    if (user) {
      return Promise.resolve({
        user,
        type
      });
    }
    return Promise.reject('User does not exist');
  } catch (err) {
    return Promise.reject(err.message);
  }
}

export function checkPermissions(user: Object, resource: string, operation: string) {
  // WARNING: Bypasses all permissions
  return true;
  /* if (!user || !user.permissions) {
    throw new Error('Token not provided or user does not exist');
  }
  const permissions = head(user.permissions.filter(item => item.resource === resource));
  if (!permissions) {
    throw new Error('You do not have permissions to access this resource');
  }
  return permissions.operations.indexOf(operation) > -1; */
}

export function checkUser(context: Object, allowedUsers: array) {
  if (context) {
    if (allowedUsers.indexOf(context.type) === -1) {
      throw new Error('You do not have permissions to access this resource');
    }
  } else {
    throw new Error('You do not have permissions to access this resource');
  }
}
