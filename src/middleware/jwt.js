import { verifyTokenAndGetUser } from '../lib/auth';

export default async function checkToken(req, res, next) {
  const token = req.get('Authorization') || req.query.token;
  if (token) {
    try {
      const { user, type } = await verifyTokenAndGetUser(token);
      req.user = user;
      req.type = type;
    } catch (e) {
      res.json({
        message: 'Invalid Token'
      });
    }
  }
  next();
}
