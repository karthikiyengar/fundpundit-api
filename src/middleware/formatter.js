// @flow
/**
 * This middleware extends the Response object and provides custom methods for a uniform JSON response. 
 */
export default function responseFormatter(req, res, next) {
    res.ok = function (data) {
        res.status(200).send(data);  
    };
    res.created = function (data) {
        res.status(201).send(data);
    };
    res.deleted = function () {
        res.status(204).send();
    };
    res.unauthorized = function (message = "Unauthorized") {
        res.status(401).send({
            message
        });
    };
    res.badRequest = function (message: string = "Request contains invalid data", errors: Array<Object>) {
        res.status(400).send({
            message,
            errors
        });
    };
    res.notFound = function (message: string = "Resource not found") {
        res.status(404).send({
            message
        });
    };
    next();
}