import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import mime from 'mime-types';
import crypto from 'crypto';
import paramValidation from './../../lib/param-validation';
import { checkUser } from './../../lib/auth';
import { userTypes } from './../../lib/constants';
import Questions from './question';
import User, { createUser } from './user';
import Bank from './bank';
import Branch from './branch';
import City from './city';
import Subproduct from './subproduct';
import CityAdmin from './city-admin';
import SiteCityAdmin from './site-city-admin';
import SiteAdmin from './site-admin';
import { sendTestimonalEmail, uploadPdfAndGetUrl, emailKyrStatus, sendNewKyrNoticeEmail, sendNewKyrNoticeEmailCityAdmin, sendKyrRejectNoticeEmailCityAdmin, escalateMailToAdmin } from '../../lib/util';

// Mongoose Schema

const kyrSchema = new mongoose.Schema({
  responses: [{
    question: String,
    option: String,
    optionId: mongoose.Schema.Types.ObjectId,
    questionid: mongoose.Schema.Types.ObjectId
  }],
  score: Number,
  subproductId: mongoose.Schema.Types.ObjectId,
  branchId: mongoose.Schema.Types.ObjectId,
  userId: mongoose.Schema.Types.ObjectId,
  amountId: mongoose.Schema.Types.ObjectId,
  cityId: mongoose.Schema.Types.ObjectId,
  bankId: mongoose.Schema.Types.ObjectId,
  sectorId: mongoose.Schema.Types.ObjectId,
  stageId: mongoose.Schema.Types.ObjectId,
  subproduct: String,
  amount: String,
  sector: String,
  stage: String,
  hasBankCalled: Boolean,
  hasBankApproved: Boolean,
  hasBankUploaded: Boolean,
  hasUserUploaded: Boolean,
  hasBankViewed: Boolean,
  rejectionReason: String,
  approvalDocument: String,
  approvalDocumentPublicId: String,
  userDocument: String,
  userDocumentPublicId: String,
  kyrDocument: String,
  kyrDocumentPublicId: String,
  completionDate: Date,
  audit: Array,
  transferAudit: [{
    transferredBy: String,
    transferredById: mongoose.Schema.Types.ObjectId,
    transferredByType: String,
    transferredFromBranch: String,
    transferredFromBranchId: mongoose.Schema.Types.ObjectId,
    transferredFromBank: String,
    transferredFromBankId: mongoose.Schema.Types.ObjectId,
    transferredFromKyr: mongoose.Schema.Types.ObjectId,
    transferredOn: Date
  }]
}, {
  timestamps: true
});

const Kyr = mongoose.model('Kyr', kyrSchema);

export const Schema = `
  type Kyr {
    id: ID
    responses: [Response]
    otherBranches: [ID]
    grade: String
    meaning: String
    recommendation: String
    score: Float
    subproduct: Subproduct
    branch: Branch
    user: User
    amount: String
    amountId: ID
    sector: String
    sectorId: ID
    stage: String
    stageId: ID
    cityId: ID
    bankId: ID
    hasBankCalled: Boolean
    hasBankApproved: Boolean
    hasBankUploaded: Boolean
    hasUserUploaded: Boolean
    hasBankViewed: Boolean
    rejectionReason: String
    approvalDocument: String
    userDocument: String
    kyrDocument: String
    kyrDocumentPublicId: String
    completionDate: String
    transferAudit: String
    createdAt: String
  }
  type Response {
    questionId: ID
    optionId: ID
    question: String
    option: String
  }
  type TestResponse {
    url: String
  }
  type CitySummary {
    city: City
    all: Int,
    approved: Int
    rejected: Int
    underProcess: Int
    daysToProcess: Float
    yetToView: Int
    new: Int
  }
  input ResponseArg {
    questionId: ID
    optionId: ID
  }
  input NewUserArg {
    firstName: String
    lastName: String
    email: String
    phoneNumber: String
  }
`;
export const Resolvers = {
  CitySummary: {
    city: ({ _id }) => City.findById(_id),
  },
  Kyr: {
    id(result) {
      return result._id;
    },
    grade(result) {
      return getGrade(result.score).grade;
    },
    meaning(result) {
      return getGrade(result.score).meaning;
    },
    recommendation(result) {
      return getGrade(result.score).recommendation;
    },
    subproduct(result) {
      return Subproduct.findById(result.subproductId);
    },
    branch(result) {
      return Branch.findById(result.branchId);
    },
    user(result) {
      return User.findById(result.userId);
    },
    responses(result) {
      return result.responses;
    },
    transferAudit(result) {
      if (result.transferAudit) {
        const { transferredBy, transferredFromBranch, transferredByType, transferredFromBank, transferredOn } = result.transferAudit;
        try {
          return `Transferred by ${transferredBy} (${transferredByType.toUpperCase()}) from ${transferredFromBank} - ${transferredFromBranch} on ${transferredOn.toLocaleString()}`;
        } catch (err) {
          return null;
        }
      }
      return null;
    }
  }
};

export const Queries = {
  kyr(parent, args) {
    checkUser(context, ['site-admin', 'user', 'bank-admin', 'city-admin', 'site-city-admin']);
    return Kyr.findById(args.id);
  },
  allKyrsForCity(parent, args, context) {
    checkUser(context, ['site-admin', 'bank-admin', 'city-admin', 'site-city-admin']);
    let { bankId, cityId } = args;
    const { filter } = args;
    if (context.type === userTypes.BANK_ADMIN) {
      bankId = context.user.bankId;
    }
    if (context.type === userTypes.CITY_ADMIN) {
      bankId = context.user.bankId;
      cityId = context.user.cityId;
    }
    switch (filter) {
      case 'all':
        return Kyr.find({ bankId, cityId }).sort({ createdAt: -1 });
      case 'underProcess':
        return Kyr.find({
          bankId,
          cityId,
          $and: [{
            $or: [{
              hasBankUploaded: true
            }, {
              hasUserUploaded: true
            }],
          }, {
            hasBankApproved: {
              $exists: false
            }
          }]
        }).sort({ createdAt: -1 });
      case 'approved':
        return Kyr.find({
          bankId,
          cityId,
          hasBankApproved: true
        }).sort({ createdAt: -1 });
      case 'rejected':
        return Kyr.find({
          bankId,
          cityId,
          hasBankApproved: false
        }).sort({ createdAt: -1 });
      case 'new':
        return Kyr.find({
          bankId,
          cityId,
          $and: [
            { hasBankUploaded: { $ne: true } },
            { hasBankApproved: { $exists: false } },
            { hasUserUploaded: { $ne: true } }
          ]
        }).sort({ createdAt: -1 });
      default:
        return Kyr.find({ bankId, cityId }).sort({ createdAt: -1 });
    }
  },
  async cityKyrSummary(parent, args, context) {
    checkUser(context, ['site-admin', 'bank-admin', 'city-admin', 'site-city-admin']);
    const query = [
      {
        $project: {
          numHasBankApproved: { $cond: ['$hasBankApproved', 1, 0] },
          numHasBankRejected: { $cond: [{ $eq: ['$hasBankApproved', false] }, 1, 0] },
          numNew: {
            $cond: [{
              $and: [{ $ne: ['$hasBankUploaded', true] }, { $ne: ['$hasUserUploaded', true] }, { $and: [{ $ne: ['$hasBankApproved', true] }, { $ne: ['$hasBankApproved', false] }] }]
            }, 1, 0]
          },
          numDaysToProcess: { $divide: [{ $subtract: ['$completionDate', '$createdAt'] }, 86400000] },
          numUnderProcess: {
            $cond: [{
              $and: [{
                $or: [{
                  $eq: ['$hasBankUploaded', true]
                }, {
                  $eq: ['$hasUserUploaded', true]
                }],
              }, {
                $and: [{ $ne: ['$hasBankApproved', true] }, { $ne: ['$hasBankApproved', false] }]
              }]
            }, 1, 0]
          },
          numYetToView: {
            $cond: [{ $ne: ['$hasBankViewed', true] }, 1, 0]
          },
          cityId: '$cityId'
        }
      }, {
        $group: {
          _id: '$cityId',
          all: { $sum: 1 },
          approved: { $sum: '$numHasBankApproved' },
          rejected: { $sum: '$numHasBankRejected' },
          underProcess: { $sum: '$numUnderProcess' },
          daysToProcess: { $avg: '$numDaysToProcess' },
          yetToView: { $sum: '$numYetToView' },
          new: { $sum: '$numNew' }
        }
      }
    ];

    let { cityId, bankId } = args;
    if (context.type === userTypes.CITY_ADMIN) {
      cityId = context.user.cityId;
      bankId = context.user.bankId;
    }
    if (context.type === userTypes.BANK_ADMIN) {
      bankId = context.user.bankId;
    }
    const match = {};
    if (cityId) {
      match.cityId = mongoose.Types.ObjectId(cityId);
    }
    if (bankId) {
      match.bankId = mongoose.Types.ObjectId(bankId);
    }
    if (cityId || bankId) {
      query.unshift({
        $match: match
      });
    }
    return Kyr.aggregate(query);
  },
  async suggestOtherBranches(parent, args, context) {
    let { subproduct, city, branch, amount, sector, stage } = args;
    const { kyrId } = args;
    if (kyrId) {
      const kyrObj = await Kyr.findById(kyrId);
      subproduct = kyrObj.subproductId;
      branch = kyrObj.branchId;
      city = kyrObj.cityId;
      amount = kyrObj.amountId;
      sector = kyrObj.sectorId;
      stage = kyrObj.stageId;
    }
    const searchQuery = {};
    if (amount) {
      searchQuery['criteria.amount'] = amount;
    }
    if (sector) {
      searchQuery['criteria.sector'] = sector;
    }
    if (stage) {
      searchQuery['criteria.stage'] = stage;
    }

    return Bank.find({
      subproduct, // where subproduct matches
      ...searchQuery
    }).select('_id')
      .then(res => Branch.find({
        bankId: {
          $in: res
        },
        cityId: city,
        _id: {
          $ne: branch
        }
      }));
  }
};

function getGrade(score) {
  if (score >= 90) {
    return {
      grade: 'AAA',
      meaning: 'Financial Instituttes are looking for clients like you.',
      recommendation: 'Bargain and get the best.'
    };
  } else if (score >= 80) {
    return {
      grade: 'AA',
      meaning: 'You are prefered client for most of the Financial Instituttes.',
      recommendation: 'Please get the right source and bargain for best offer.'
    };
  } else if (score >= 70) {
    return {
      grade: 'A',
      meaning: 'You might be a preferred client for few of the financial instittutes.',
      recommendation: 'You may appoint a financial advisor, just to get the best offer.'
    };
  } else if (score >= 60) {
    return {
      grade: 'BBB',
      meaning: 'Good deal for investment but might not be interested to every investor.',
      recommendation: 'Need to appoint a financial advisor to get a better deal.'
    };
  } else if (score >= 50) {
    return {
      grade: 'BB',
      meaning: 'Potential of investment in proposal but should be presented properly.',
      recommendation: 'Need to appoint a Financial Advisor.'
    };
  } else if (score >= 40) {
    return {
      grade: 'B',
      meaning: 'You might face difficulties in getting funds.',
      recommendation: 'Need to appoint a Financial Advisor who has earlier done similar kind of work or better than that.'
    };
  } else if (score >= 30) {
    return {
      grade: 'C',
      meaning: 'Less chances of getting funds.',
      recommendation: 'Need to hire the specific consultant to get the funding with a little improvement in profile.'
    };
  }
  return {
    grade: 'D',
    meaning: 'Not a right time to get the funding.',
    recommendation: 'Please imrpove your profile to upgrade the rating.'
  };
}

export const Mutations = {
  async uploadUserDocument(parent, args, context) {
    checkUser(context, ['site-admin', 'user', 'bank-admin', 'city-admin', 'site-city-admin']);
    const { file, id } = args;
    const kyr = await Kyr.findById(id);
    if (kyr) {
      const audit = {
        action: 'hasUserUploaded',
        value: true,
        updatedAt: Date.now(),
        updatedBy: context.user._id,
        updatedByType: context.type,
        updatedByName: `${context.user.firstName} ${context.user.lastName}`
      };
      const ext = mime.extension(file.split(';')[0].split(':')[1]);
      const upload = await cloudinary.v2.uploader.upload(file, { public_id: `${context.user.firstName}${context.user.lastName}-UserDocument-${crypto.randomBytes(5).toString('hex')}.${ext}`, resource_type: 'raw', unique_filename: true });
      kyr.userDocument = upload.url;
      kyr.userDocumentPublicId = upload.public_id;
      kyr.hasUserUploaded = true;
      kyr.audit = kyr.audit ? [audit, ...kyr.audit] : [audit];
      return kyr.save();
    }
    throw Error('KYR does not exist');
  },
  async transferKyr(parent, args, context) {
    checkUser(context, ['site-admin', 'user', 'site-city-admin']);
    const { kyrId, otherBranches } = args;
    const kyr = await Kyr.findById(kyrId);
    const originalBranch = await Branch.findById(kyr.branchId);
    const originalBank = await Bank.findById(kyr.bankId);
    const response = [];
    for (const id of otherBranches) {
      const branch = await Branch.findById(id);
      const newKyr = new Kyr({
        userId: kyr.userId,
        subproductId: kyr.subproductId,
        subproduct: kyr.subproduct,
        amount: kyr.amount,
        cityId: branch.cityId,
        bankId: branch.bankId,
        amountId: kyr.amountId,
        stageId: kyr.stageId,
        stage: kyr.stage,
        sectorId: kyr.sectorId,
        kyrDocument: kyr.kyrDocument,
        kyrDocumentPublicId: kyr.kyrDocumentPublicId,
        sector: kyr.sector,
        score: kyr.score,
        branchId: id,
        responses: kyr.responses,
        transferAudit: {
          transferredBy: `${context.user.firstName} ${context.user.lastName}`,
          transferredById: context.user._id,
          transferredByType: context.type,
          transferredFromBranchId: kyr.branchId,
          transferredFromBranch: originalBranch.location,
          transferredFromBankId: kyr.bankId,
          transferredFromBank: originalBank.name,
          transferredFromKyr: kyr._id,
          transferredOn: Date.now()
        }
      });
      const kyrObject = await newKyr.save();

      response.push({
        ...kyrObject.toJSON(),
        ...getGrade(kyrObject.score)
      });
      const cityAdmins = await CityAdmin.find({ bankId: branch.bankId, cityId: branch.cityId });
      cityAdmins.forEach(async (admin) => {
        if (admin.email) {
          sendNewKyrNoticeEmail(admin.email, admin.firstName);
        }
      });
    }
    return response;
  },
  async setKyrFlag(parent, args, context) {
    checkUser(context, ['site-admin', 'user', 'bank-admin', 'city-admin', 'site-city-admin']);
    const { kyrId, flag, value, reason, file } = args;
    const kyr = await Kyr.findById(kyrId);
    const user = await User.findById(kyr.userId);
    const name = `${user.firstName} ${user.lastName}`;
    if (kyr) {
      const audit = {
        action: flag,
        value,
        updatedAt: Date.now(),
        updatedBy: context.user._id,
        updatedByType: context.type,
        updatedByName: `${context.user.firstName} ${context.user.lastName}`
      };
      kyr[flag] = value;
      emailKyrStatus(user.email, name);
      if (flag === 'hasBankApproved') {
        kyr.completionDate = Date.now();
        kyr.save();
        sendTestimonalEmail(user.email, name, kyr.branchId, user.id);
      }
      if (reason) {
        kyr.rejectionReason = reason;
        kyr.completionDate = Date.now();
        sendTestimonalEmail(user.email, name, kyr.branchId, user.id);
        const siteCityAdmins = await SiteCityAdmin.find({ cityId: kyr.cityId });
        for (const admin of siteCityAdmins) {
          sendKyrRejectNoticeEmailCityAdmin(admin.email, admin.firstName, kyr.userId);
        }
      }
      if (file) {
        const ext = mime.extension(file.split(';')[0].split(':')[1]);
        const upload = await cloudinary.v2.uploader.upload(file, { public_id: `${name}-BankDocument-${crypto.randomBytes(5).toString('hex')}.${ext}`, resource_type: 'raw', unique_filename: true });
        kyr.approvalDocument = upload.url;
        kyr.approvalDocumentPublicId = upload.public_id;
      }
      kyr.audit = kyr.audit ? [audit, ...kyr.audit] : [audit];
      return kyr.save();
    }
    throw Error('KYR does not exist');
  },
  async submitKyr(parent, args, context) {
    await Joi.validate(args, paramValidation.submitKyr);
    const { subproductId, branchId, amountId, sectorId, stageId, responses, otherBranches } = args;
    const questionIds = responses.map(response => response.questionId);
    const [subproduct, branch, questions] = await Promise.all([Subproduct.findById(subproductId), Branch.findById(branchId), Questions.find({
      _id: {
        $in: questionIds
      }
    })]);
    const amountField = subproduct.fields.find(item => item.key === 'amount');

    let sector;
    let stage;
    if (sectorId) {
      const sectorField = subproduct.fields.find(item => item.key === 'sector');
      sector = sectorField.options.find(item => item._id.equals(sectorId)).label; // store denormalized sector
    }
    if (stageId) {
      const stageField = subproduct.fields.find(item => item.key === 'stage');
      stage = stageField.options.find(item => String(item._id) === String(stageId)).label; // store denormalized sector
    }
    const amount = amountField.options.find(item => String(item._id) === String(amountId)).label; // store denormalized amount
    let user;
    // if token does not exist, create a new user
    if (args.user) {
      user = await createUser(args.user);
    } else {
      user = context.user;
    }

    const score = questions.map((question) => {
      question = question.toJSON();
      const response = responses.find(item => question._id.equals(item.questionId));
      const option = question.options.find(item => item._id.equals(response.optionId));
      response.option = option.label; // store denomalized labels
      response.question = question.question; // store denomalized labels
      if (option) {
        return option.value * question.weight;
      }
      throw new Error(`Invaid option chosen for question ${question._id}: ${question.question}`);
    }).reduce((a, b) => Number(a) + Number(b), 0);

    const response = [];

    // save original kyr
    const originalKyr = {
      userId: user._id,
      subproductId,
      subproduct: subproduct.label,
      amount,
      cityId: branch.cityId,
      bankId: branch.bankId,
      amountId,
      stageId,
      stage,
      sectorId,
      sector,
      score,
      branchId,
      responses
    };
    const grades = getGrade(originalKyr.score);
    const originalKyrDocument = await uploadPdfAndGetUrl(originalKyr, user, grades);
    const kyr = new Kyr({
      ...originalKyr,
      kyrDocument: originalKyrDocument.url,
      kyrDocumentPublicId: originalKyrDocument.public_id,
    });
    const kyrObject = await kyr.save();
    response.push({
      ...kyrObject,
      ...grades
    });

    SiteCityAdmin.find({ cityId: branch.cityId })
      .then((siteCityAdmins) => {
        for (const admin of siteCityAdmins) {
          sendNewKyrNoticeEmailCityAdmin(admin.email, admin.firstName, user._id);
        }
      });

    CityAdmin.find({ bankId: branch.bankId, cityId: branch.cityId })
      .then((cityAdmins) => {
        cityAdmins.forEach(async (admin) => {
          sendNewKyrNoticeEmail(admin.email, admin.firstName);
        });
      });

    SiteAdmin.find({})
      .then((siteAdmins) => {
        for (const admin of siteAdmins) {
          sendNewKyrNoticeEmailCityAdmin(admin.email, admin.firstName, user._id);
        }
      });

    if (otherBranches && otherBranches.length > 0) {
      for (const id of otherBranches) {
        const currentBranch = await Branch.findById(id);
        const currentBank = await Bank.findById(currentBranch.bankId);
        const currentKyr = new Kyr({
          userId: user._id,
          subproductId,
          subproduct: subproduct.label,
          amount,
          cityId: currentBranch.cityId,
          bankId: currentBranch.bankId,
          amountId,
          sector,
          sectorId,
          stage,
          stageId,
          score,
          branchId: id,
          responses,
          transferAudit: {
            transferredBy: `${user.firstName} ${user.lastName}`,
            transferredById: user._id,
            transferredByType: userTypes.USER,
            transferredFromBranchId: kyrObject.branchId,
            transferredFromBranch: branch.location,
            transferredFromBankId: branch.bankId,
            transferredFromBank: currentBank.name,
            transferredFromKyr: kyrObject._id,
            transferredOn: Date.now()
          },
          kyrDocument: kyrObject.kyrDocument,
          kyrDocumentPublicId: kyrObject.kyrDocumentPublicId,
        });
        const result = await currentKyr.save();
        response.push({
          ...result,
          ...grades
        });
        CityAdmin.find({ bankId: branch.bankId, cityId: branch.cityId })
          .then((cityAdmins) => {
            cityAdmins.forEach(async (admin) => {
              sendNewKyrNoticeEmail(admin.email, admin.firstName);
            });
          });
      }
    }
    return response;
  },
  async escalateToAdmin(parent, args, context) {
    checkUser(context, ['user']);
    const kyr = await Kyr.findById(args.kyrId);
    if (kyr) {
      const siteCityAdmins = await SiteCityAdmin.find({ cityId: kyr.cityId });
      const bank = await Bank.findById(kyr.bankId);
      const user = await User.findById(kyr.userId);
      const subproduct = await Subproduct.findById(kyr.subproductId);
      const userFullName = `${user.firstName} ${user.lastName}`;
      for (const admin of siteCityAdmins) {
        escalateMailToAdmin(admin.email, admin.firstName, userFullName, bank.name, user.email, user.phoneNumber, subproduct.label, kyr.amount, kyr.userId);
      }
      const siteAdmins = await SiteAdmin.find({});
      for (const admin of siteAdmins) {
        escalateMailToAdmin(admin.email, admin.firstName, userFullName, bank.name, user.email, user.phoneNumber, subproduct.label, kyr.amount, kyr.userId);
      }
      return kyr;
    }
    throw Error('KYR does not exist');
  }
};

export default Kyr;
