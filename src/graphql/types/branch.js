import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import { userTypes } from './../../lib/constants';
import Bank from './bank';
import City from './city';
import Kyr from './kyr';
import Testimonial from './testimonial';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const branchSchema = new mongooseSchema({
  address: { type: String, unique: true },
  location: { type: String },
  cityId: mongooseSchema.Types.ObjectId,
  branchEmail: { type: String },
  bankId: mongooseSchema.Types.ObjectId,
  latitude: { type: String },
  longitude: { type: String },
  rating: { type: String },
  contactNumber: { type: String },
}, { timestamps: true });


const Branch = mongoose.model('Branch', branchSchema);

export const Schema = `
    type Branch {
        id: ID
        address: String
        location: String
        branchEmail: String
        bankId: ID
        bank: Bank
        cityId: ID
        city: City
        latitude: String
        longitude: String
        rating: String
        contactNumber: String
        kyr: [Kyr]
        reviewCount: Int
        rating: Float
    }
`;

export const Resolvers = {
  Branch: {
    id(parent) {
      return parent._id.toString();
    },
    bank(parent) {
      return Bank.findById(parent.bankId);
    },
    city(parent) {
      return City.findById(parent.cityId);
    },
    kyr(parent, args, context) {
      if (context.type == null || context.type === userTypes.USER) return null;
      return Kyr.find({ branchId: parent._id }).sort({ createdAt: -1 });
    },
    async reviewCount(parent) {
      return Testimonial.count({ branchId: parent._id });
    },
    async rating(parent) {
      const rating = await Testimonial.aggregate([
        {
          $match: {
            branchId: parent._id
          }
        },
        {
          $group: {
            _id: null,
            rating: { $avg: '$rating' }
          }
        }
      ]);
      return rating.length > 0 ? rating[0].rating : null;
    }
  }
};

export const Queries = {
  async allBranches(parent, args, context) {
    checkUser(context, ['site-admin']);
    return Branch.find({});
  },
  async branch(parent, args, context) {
    const branch = await Branch.findById(args.id);
    if (branch) {
      return branch;
    }
    throw Error('Branch does not exist');
  }
};

export const Mutations = {
  async addBranch(parent, args, context) {
    await Joi.validate(args, paramValidation.addBranch);
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin', 'city-admin']);
    const { address, location, branchEmail, cityId, latitude, longitude, contactNumber } = args;
    let { bankId } = args;
    if (context.type === userTypes.BANK_ADMIN) {
      bankId = context.user.bankId;
    }
    if (!bankId) {
      throw Error('Invalid Bank ID');
    }
    const branch = new Branch({
      address,
      location,
      branchEmail,
      bankId,
      cityId,
      latitude,
      longitude,
      contactNumber,
    });
    return branch.save();
  },
  async updateBranch(parent, args, context) {
    await Joi.validate(args, paramValidation.updateBranch);
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin', 'city-admin']);
    const { id, address, location, branchEmail, bankId, cityId, latitude, longitude, contactNumber, email } = args;
    const branch = await Branch.findById(id);
    if (branch) {
      branch.address = address || branch.address;
      branch.location = location || branch.location;
      branch.branchEmail = branchEmail || branch.branchEmail;
      branch.bankId = bankId || branch.bankId;
      branch.cityId = cityId || branch.cityId;
      branch.latitude = latitude || branch.latitude;
      branch.longitude = longitude || branch.longitude;
      branch.contactNumber = contactNumber || branch.contactNumber;
      branch.email = email || branch.email;
      return branch.save();
    }
    throw Error('Branch does not exist.');
  },
  async deleteBranch(parent, args, context) {
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin', 'city-admin']);
    const { id } = args;
    const branch = await Branch.findById(id);
    if (branch) {
      return branch.remove();
    }
    throw Error('Branch does not exist.');
  }
};

export default Branch;
