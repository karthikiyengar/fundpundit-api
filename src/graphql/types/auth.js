import Joi from 'joi';
import { v4 } from 'uuid';
import modelMap from '../../lib/modelMap';
import { authenticateAndGenerateToken, generateSalt, sha512, checkPermissions } from '../../lib/auth';
import { sendForgotPasswordMail } from '../../lib/util';
import paramValidation from '../../lib/param-validation';

export const Schema = `
  type LoginResponse {
    token: String
    user: User
  }
`;

export const Queries = {
  async checkTokenValidity(parent, args) {
    const { token, user, type } = args;
    const userObject = await modelMap[type].findById(user);
    if (!userObject) {
      return {
        valid: false
      };
    }
    if (token === userObject.verificationToken && (userObject.isVerified === false || userObject.forgotPassword === true)) {
      return {
        valid: true,
        isPasswordSet: userObject.password !== undefined,
        isForgotPassword: userObject.forgotPassword
      };
    }
    return {
      valid: false
    };
  }
};

export const Mutations = {
  async login(parent, args) {
    await Joi.validate(args, paramValidation.login);
    const { type, email, password } = args;
    return authenticateAndGenerateToken(type, email, password);
  },
  async forgotPassword(parent, args) {
    const { email, type } = args;
    const user = await modelMap[type].findOne({ email });
    if (!user) {
      throw Error('User does not exist');
    }
    const verificationToken = v4();
    user.verificationToken = verificationToken;
    user.forgotPassword = true;
    sendForgotPasswordMail(email, user, verificationToken);
    user.save();
    return user;
  },
  async setPassword(parent, args) {
    const { password, confirmPassword, token, type, user: userId } = args;
    const user = await modelMap[type].findById(userId);
    if (user && token === user.verificationToken && !password) {
      user.isVerified = true;
      user.forgotPassword = false;
      user.save();
      return user;
    }
    if (user && password === confirmPassword && token === user.verificationToken) {
      const salt = generateSalt();
      user.password = sha512(password, salt);
      user.salt = salt;
      user.isVerified = true;
      user.forgotPassword = false;
      user.save();
      return user;
    }
    throw new Error('Invalid Arguments');
  },
  async updatePassword(parent, args, context) {
    await Joi.validate(args, paramValidation.updatePassword);
    if (checkPermissions(context.user, 'user', 'update')) {
      const { currentPassword, password, confirmPassword, } = args;
      if (password !== confirmPassword) {
        throw Error('Password and confirm password should be same.');
      }
      const oldHash = sha512(currentPassword, context.user.salt);
      const newHash = sha512(password, context.user.salt);
      if (context.user.password === oldHash && oldHash !== newHash) {
        const newSalt = generateSalt();
        context.user.password = sha512(password, newSalt);
        context.user.salt = newSalt;
        return context.user.save();
      }
      throw Error('Incorrect current password or new password same as old password');
    }
    return null;
  }
};

export const Resolvers = {
  LoginResponse: {
    user(parent) {
      return parent.user;
    }
  }
};
