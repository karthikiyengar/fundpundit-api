import mongoose from 'mongoose';
import { v4 } from 'uuid';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import City from './city';
import Bank from './bank';
import Permissions from './permission.js';
import { checkUser, generateSalt, sha512 } from '../../lib/auth';
import { userTypes } from '../../lib/constants';
import { sendCityAdminVerificationEmail } from '../../lib/util';
/**
 * Mongoose Schema
 */
const mongooseSchema = mongoose.Schema;
const cityAdminSchema = new mongooseSchema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  salt: String,
  password: String,
  cityId: { type: mongooseSchema.Types.ObjectId },
  bankId: mongooseSchema.Types.ObjectId,
  permissions: Array,
  verificationToken: String,
  isVerified: { type: Boolean, default: false }
}, {
  timestamps: true
});

const CityAdmin = mongoose.model('CityAdmin', cityAdminSchema);

/**
 * GraphQL Types
 */
export const Schema = `
  type CityAdmin {
    id: ID
    firstName: String
    lastName: String
    fullName: String
    email: String
    cityId: ID
    bankId: ID
    bank: Bank
    city: City
    isVerified: Boolean
    verificationToken: String
  }
`;

export const Resolvers = {
  CityAdmin: {
    id: parent => parent._id,
    city: parent => City.findById(parent.cityId),
    bank: parent => Bank.findById(parent.bankId),
    fullName: parent => `${parent.firstName} ${parent.lastName}`
  }
};

export const Queries = {
  async cityAdmin(parent, args, context) {
    if (context.user && context.type === userTypes.CITY_ADMIN) {
      return context.user;
    }
    return null;
  },
  async cityAdmins(_, { bankId }, context) {
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin']);
    if (context.type === userTypes.BANK_ADMIN) {
      bankId = context.user.bankId;
    }
    return CityAdmin.find({ bankId: mongoose.Types.ObjectId(bankId) });
  }
};

export const Mutations = {
  async addCityAdmin(parent, args, context) {
    await Joi.validate(args, paramValidation.addCityAdmin);
    const permissions = await Permissions.findOne({ type: userTypes.CITY_ADMIN });
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin']);
    const { firstName, lastName, email, cityId, password } = args;
    let { bankId } = args;
    if (context.type === userTypes.BANK_ADMIN) {
      bankId = context.user.bankId;
    }
    const verificationToken = v4();
    const payload = {
      firstName,
      lastName,
      email,
      cityId,
      bankId,
      verificationToken,
      permissions: permissions ? permissions.roles : null
    };
    if (password) {
      const salt = generateSalt();
      payload.salt = salt;
      payload.password = sha512(password, salt);
    }
    const cityAdmin = new CityAdmin(payload);
    const cityAdminObject = await cityAdmin.save();
    sendCityAdminVerificationEmail(email, firstName, cityAdminObject.toJSON()._id, verificationToken);
    return cityAdminObject;
  },
  async updateCityAdmin(parent, args, context) {
    await Joi.validate(args, paramValidation.updateCityAdmin);
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin', 'city-admin']);
    const { id, firstName, lastName } = args;
    const cityAdmin = await CityAdmin.findById(id);
    if (cityAdmin) {
      cityAdmin.firstName = firstName || cityAdmin.firstName;
      cityAdmin.lastName = lastName || cityAdmin.lastName;
      return cityAdmin.save();
    }
    throw Error('City admin does not exist');
  },
  async deleteCityAdmin(_, { id }, { user }) {
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin']);
    const cityAdmin = await CityAdmin.findById(id);
    if (cityAdmin) {
      return cityAdmin.remove();
    }
    throw Error('City admin does not exist.');
  }
};

export default CityAdmin;
