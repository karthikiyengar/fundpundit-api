// @flow
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const permissionSchema = new Schema({
  type: String,
  roles: Array
});

const Permission = mongoose.model('permission', permissionSchema);

export default Permission;
