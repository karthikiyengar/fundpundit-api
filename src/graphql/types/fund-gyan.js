import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const fundGyanSchema = new mongooseSchema({
  label: {
    type: String,
    required: true,
    unique: true
  },
  image: { type: String },
  imagePublicId: { type: String },
}, { timestamps: true });

const FundGyan = mongoose.model('FundGyan', fundGyanSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type FundGyan {
        id: ID
        label: String
        image: String
        imagePublicId: String
    }
`;

export const Resolvers = {
  FundGyan: {
    id(parent) {
      return parent._id;
    }
  }
};

export const Queries = {
  async allFundGyans() {
    return FundGyan.find({});
  },
  async fundGyan(parent, args) {
    return FundGyan.findById(args.id);
  },
};

export const Mutations = {
  async addFundGyan(parent, args, context) {
    await Joi.validate(args, paramValidation.addFundGyan);
    checkUser(context, ['site-admin']);
    const { label, image } = args;
    const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
    const fundGyan = new FundGyan({
      label,
      image: uploaded.url,
      imagePublicId: uploaded.public_id,
    });
    return fundGyan.save();
  },
  async updateFundGyan(parent, args, context) {
    await Joi.validate(args, paramValidation.updateFundGyan);
    checkUser(context, ['site-admin']);
    const { id, label, image } = args;
    const fundGyan = await FundGyan.findById(id);
    if (fundGyan) {
      fundGyan.label = label || fundGyan.label;
      if (image) {
        cloudinary.v2.uploader.destroy(fundGyan.imagePublicId);
        const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
        fundGyan.image = uploaded.url;
        fundGyan.imagePublicId = uploaded.public_id;
      }
      return fundGyan.save();
    }
    throw Error('FundGyan does not exist.');
  },
  async deleteFundGyan(parent, args, context) {
    checkUser(context, ['site-admin']);
    const fundGyan = await FundGyan.findById(args.id);
    if (fundGyan) {
      return fundGyan.remove();
    }
    throw Error('Fund gyan does not exist.');
  }
};

export default FundGyan;
