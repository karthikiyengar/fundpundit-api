import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';
import City from './city';
import Subproduct from './subproduct';
import SiteCityAdmin from './site-city-admin';
import SiteAdmin from './site-admin';
import { sendNewPopUserNoticeEmail } from '../../lib/util';

const mongooseSchema = mongoose.Schema;
const popupUserSchema = new mongooseSchema({
  fullName: { type: String, required: true },
  email: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  subproductId: { type: mongoose.Schema.Types.ObjectId, required: true },
  subproduct: { type: String, required: true },
  amountId: { type: mongoose.Schema.Types.ObjectId, required: true },
  amount: { type: String, required: true },
  sector: String,
  sectorId: mongoose.Schema.Types.ObjectId,
  stage: String,
  stageId: mongoose.Schema.Types.ObjectId,
  cityId: { type: mongoose.Schema.Types.ObjectId, required: true },
  city: { type: String, required: true }
}, { timestamps: true });

const PopupUser = mongoose.model('popupUser', popupUserSchema);

export const Schema = `
    type PopupUser {
      id: ID
      fullName: String
      email: String
      phoneNumber: String
      subproduct: String
      amount: String
      sector: String
      stage: String
      city: String
      createdAt: String
      cityId: ID
    }
`;

export const Resolvers = {
  PopupUser: {
    id(parent) {
      return parent._id;
    }
  }
};

export const Queries = {
  async allPopupUsers(parent, args, context) {
    
    checkUser(context, ['site-admin', 'site-city-admin']);
    let query = {};
    if(context.user.cityId){
      query.cityId = context.user.cityId;
    }
    console.log(context.user.cityId);
    return await PopupUser.find(query).sort({ createdAt: -1 });
  }
};

export const Mutations = {
  async addPopupUser(parent, args) {
    await Joi.validate(args, paramValidation.addPopupUser);
    const { fullName, email, phoneNumber, subproductId, amountId, stageId, sectorId, cityId } = args;
    const subproduct = await Subproduct.findById(subproductId);
    let stage;
    let sector;
    if (sectorId) {
      const sectorField = subproduct.fields.find(item => item.key === 'sector');
      sector = sectorField.options.find(item => item._id.equals(sectorId)).label; // store denormalized sector
    }
    if (stageId) {
      const stageField = subproduct.fields.find(item => item.key === 'stage');
      stage = stageField.options.find(item => item._id.equals(stageId)).label; // store denormalized sector
    }
    const amountField = subproduct.fields.find(item => item.key === 'amount');
    const amount = amountField.options.find(item => item._id.equals(amountId)).label; // store denormalized amount

    const city = await City.findById(cityId);

    const siteAdmins = await SiteAdmin.find({});
    for (const admin of siteAdmins) {
      sendNewPopUserNoticeEmail(admin.email, admin.firstName, fullName, email, phoneNumber, subproduct.label, amount, city.name);
    }

    const siteCityAdmins = await SiteCityAdmin.find({ cityId });
    for (const admin of siteCityAdmins) {
      sendNewPopUserNoticeEmail(admin.email, admin.firstName, fullName, email, phoneNumber, subproduct.label, amount, city.name);
    }

    const popupUser = new PopupUser({
      fullName,
      email,
      phoneNumber,
      subproductId,
      subproduct: subproduct.label,
      amountId,
      amount,
      sector,
      stage,
      stageId,
      sectorId,
      cityId,
      city: city.name
    });
    return popupUser.save();
  }
};

export default PopupUser;
