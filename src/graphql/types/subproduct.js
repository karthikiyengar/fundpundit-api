// @flow
import mongoose from 'mongoose';
import paramValidation from './../../lib/param-validation';
import { checkPermissions } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const subproductSchema = new mongooseSchema({
  label: {
    type: String,
    required: true,
    unique: true
  },
  fields: {
    type: Array
  },
  image: {
    type: String
  },
  icon: {
    type: String
  },
  default: Array,
  details: Array
}, { timestamps: true });

const Subproduct = mongoose.model('Subproduct', subproductSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type Subproduct {
      id: ID
      label: String
      image: String
      fields: [FieldsResponse]
      details: [Details]
      icon: String
    }

    type Details {
      id: ID
      label: String
      key: String
    }

    type FieldsResponse {
      id: ID
      label: String
      key: String
      options: [Option]
    }

    type Option {
      id: ID
      label: String
    }

    type Amount {
      id: String
      label: String
    }

    type Sector {
      id: String
      label: String
    }

    type Stage{
      id: String
      label: String
    }
`;

export const Resolvers = {
  FieldsResponse: {
    id(parent) {
      return parent._id;
    },
    options(parent) {
      return parent.options;
    }
  },
  Subproduct: {
    id(parent) {
      return parent._id;
    },
    fields(parent) {
      return parent.fields;
    },
    details(parent) {
      return parent.details;
    }
  },
  Details: {
    id(parent) {
      return parent._id;
    }
  }
};

export const Queries = {
  async allSubproducts() {
    return Subproduct.find({}).sort({ order: 1 });
  },
  async subproduct(parent, args) {
    const { id } = args;
    return Subproduct.findById(id).sort({ order: 1 });
  },
};

export const Mutations = {

};

export default Subproduct;
