import mongoose from 'mongoose';
import Joi from 'joi';
import { v4 } from 'uuid';
import Bank from './bank';
import permissions from './permission.js';
import paramValidation from './../../lib/param-validation';
import { checkUser, sendVerificationEmail } from '../../lib/auth';
import { userTypes } from '../../lib/constants';

/**
 * Mongoose Schema
 */
const MongooseSchema = mongoose.Schema;
const bankAdminSchema = new MongooseSchema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  salt: {
    type: String
  },
  password: {
    type: String
  },
  bankId: {
    type: MongooseSchema.Types.ObjectId
  },
  permissions: {
    type: Object
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  verificationToken: {
    type: String,
  },
  image: {
    type: String
  }
}, {
  timestamps: true
});

const BankAdmin = mongoose.model('BankAdmin', bankAdminSchema);

/**
 * GraphQL Types
 */
export const Schema = `
  type BankAdmin {
    id: ID
    firstName: String
    lastName: String
    email: String
    bankId: ID
    bank: Bank
    isVerified: Boolean
    verificationToken: String
    image: String
  }
`;

export const Resolvers = {
  BankAdmin: {
    id(parent) {
      return parent._id;
    },
    bank(parent) {
      return Bank.findById(parent.bankId);
    }
  }
};

export const Queries = {
  async bankAdmin(parent, args, context) {
    if (context.user && context.type === userTypes.BANK_ADMIN) {
      return context.user;
    }
    return null;
  },
  async bankAdmins(parent, args, context) {
    checkUser(context, ['site-admin', 'site-city-admin', 'bank-admin']);
    const { bankId } = args;
    const bankAdmin = BankAdmin.find({ bankId });
    if (bankAdmin) {
      return bankAdmin;
    }
    throw Error('Bank admins does not exist for this bank.');
  }
};

export const Mutations = {
  async addBankAdmin(parent, args, context) {
    await Joi.validate(args, paramValidation.addBankAdmin);
    const permissionRoles = await permissions.findOne({ type: userTypes.BANK_ADMIN });
    checkUser(context, ['site-admin', 'site-city-admin']);
    const { firstName, lastName, email, bankId } = args;
    const verificationToken = v4();
    const bankAdmin = new BankAdmin({
      firstName,
      lastName,
      email,
      bankId,
      permissions: permissionRoles.roles,
      verificationToken,
      image: 'https://image.freepik.com/free-vector/logo-sample-text_355-558.jpg'
    });
    const bankAdminObject = await bankAdmin.save();
    sendVerificationEmail(email, firstName, lastName, bankAdminObject.toJSON()._id, verificationToken);
    return bankAdminObject;
  },
  async updateBankAdmin(parent, args, context) {
    await Joi.validate(args, paramValidation.updateBankAdmin);
    checkUser(context, ['site-admin', 'site-city-admin']);
    const { id, firstName, lastName, email, bankId } = args;
    const bankAdmin = await BankAdmin.findById(id);
    if (bankAdmin) {
      bankAdmin.firstName = firstName || bankAdmin.firstName;
      bankAdmin.lastName = lastName || bankAdmin.lastName;
      bankAdmin.email = email || bankAdmin.email;
      bankAdmin.bankId = bankId || bankAdmin.bankId;
      return bankAdmin.save();
    }
    throw Error('Bank admin does not exist.');
  },
  async deleteBankAdmin(_, { id }, { user }) {
    checkUser(context, ['site-admin', 'site-city-admin']);
    const bankAdmin = await BankAdmin.findById(id);
    if (bankAdmin) {
      return bankAdmin.remove();
    }
    throw Error('Bank admin does not exist.');
  }
};

export default BankAdmin;
