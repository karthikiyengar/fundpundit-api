import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import Service from './service';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const companySchema = new mongooseSchema({
  companyName: { type: String },
  contactName: { type: String },
  contactNumber: { type: String },
  companyLogo: { type: String },
  imagePublicId: { type: String },
  address: { type: String },
  description: { type: String },
  serviceOffered: { type: String },
  serviceId: mongooseSchema.Types.ObjectId
}, { timestamps: true });

const Company = mongoose.model('Company', companySchema);

export const Schema = `
    type Company {
        id: ID
        companyName: String
        contactName: String
        contactNumber: String
        companyLogo: String
        address: String
        description: String
        serviceOffered: String
        serviceId: ID
        service: Service
    }
`;

export const Resolvers = {
  Company: {
    id(parent) {
      return parent._id.toString();
    },
    service(parent) {
      return Service.findById(parent.serviceId);
    }
  }
};

export const Queries = {
  async allCompanies(parent, args, context) {
    return Company.find({});
  },
  async company(parent, args, context) {
    const { id } = args;
    const company = Company.findById(id);
    if (company) {
      return company;
    }
    throw Error('Companies does not exists this Service.');
  }
};

export const Mutations = {
  async addCompany(parent, args, context) {
    await Joi.validate(args, paramValidation.addCompany);
    checkUser(context, ['site-admin']);
    const { companyName, contactName, contactNumber, companyLogo, address, description, serviceOffered, serviceId } = args;
    const uploaded = await cloudinary.v2.uploader.upload(companyLogo, { quality: 30 });
    const company = new Company({
      companyName,
      contactName,
      contactNumber,
      companyLogo: uploaded.url,
      imagePublicId: uploaded.public_id,
      address,
      description,
      serviceOffered,
      serviceId
    });
    return company.save();
  },
  async updateCompany(parent, args, context) {
    await Joi.validate(args, paramValidation.updateCompany);
    checkUser(context, ['site-admin']);
    const { id, companyName, contactName, contactNumber, address, description, serviceOffered, companyLogo, serviceId } = args;
    const company = await Company.findById(id);
    if (company) {
      if (companyLogo) {
        cloudinary.v2.uploader.destroy(company.imagePublicId);
        const uploaded = await cloudinary.v2.uploader.upload(companyLogo, { quality: 30 });
        company.companyLogo = uploaded.url;
        company.imagePublicId = uploaded.public_id;
      }
      company.companyName = companyName || company.companyName;
      company.contactName = contactName || company.contactName;
      company.contactNumber = contactNumber || company.contactNumber;
      company.address = address || company.address;
      company.description = description || company.description;
      company.serviceOffered = serviceOffered || company.serviceOffered;
      company.serviceId = serviceId || company.serviceId;
      return company.save();
    }
    throw Error('Company does not exist.');
  },
  async deleteCompany(parent, args, context) {
    checkUser(context, ['site-admin']);
    const { id } = args;
    const company = await Company.findById(id);
    if (company) {
      cloudinary.v2.uploader.destroy(company.imagePublicId);
      return company.remove();
    }
    throw Error('Company does not exist.');
  }
};

export default Company;
