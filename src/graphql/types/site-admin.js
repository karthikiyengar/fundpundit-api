// @flow
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const adminSchema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: String,
  email: {
    type: String,
    required: true
  },
  salt: {
    type: String
  },
  password: {
    type: String
  },
  isVerified: Boolean
}, { timestamps: true });

adminSchema.statics.findByEmail = function (email) {
  return this.findOne({ email });
};

const SiteAdmin = mongoose.model('SiteAdmin', adminSchema);

export default SiteAdmin;
