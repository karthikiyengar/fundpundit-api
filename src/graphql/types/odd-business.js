import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const oddBusinessSchema = new mongooseSchema({
  label: {
    type: String,
    required: true,
    unique: true
  },
  image: {
    type: String,
    required: true
  },
  imagePublicId: { type: String }
}, { timestamps: true });

const OddBusiness = mongoose.model('OddBusiness', oddBusinessSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type OddBusiness {
        id: ID
        label: String
        image: String
        imagePublicId: String
    }
`;

export const Resolvers = {
  OddBusiness: {
    id(parent) {
      return parent._id;
    }
  }
};

export const Queries = {
  async allOddBusinesses() {
    return OddBusiness.find({});
  },
  async oddBusiness(parent, args) {
    return OddBusiness.findById(args.id);
  },
};

export const Mutations = {
  async addOddBusiness(parent, args, context) {
    checkUser(context, ['site-admin']);
    await Joi.validate(args, paramValidation.addOddBusiness);
    const { label, image } = args;
    const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
    const oddBusiness = new OddBusiness({
      label,
      image: uploaded.url,
      imagePublicId: uploaded.public_id
    });
    return oddBusiness.save();
  },
  async updateOddBusiness(parent, args, context) {
    checkUser(context, ['site-admin']);
    await Joi.validate(args, paramValidation.updateOddBusiness);
    const { id, label, image } = args;
    const oddBusiness = await OddBusiness.findById(id);
    if (oddBusiness) {
      oddBusiness.label = label || oddBusiness.label;
      if (image) {
        cloudinary.v2.uploader.destroy(oddBusiness.imagePublicId);
        const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
        oddBusiness.image = uploaded.url;
        oddBusiness.imagePublicId = uploaded.public_id;
      }
      return oddBusiness.save();
    }
    throw Error('Odd Business does not exist.');
  },
  async deleteOddBusiness(parent, args, context) {
    checkUser(context, ['site-admin']);
    const oddBusiness = await OddBusiness.findById(args.id);
    if (oddBusiness) {
      cloudinary.v2.uploader.destroy(oddBusiness.imagePublicId);
      return oddBusiness.remove();
    }
    throw Error('Odd Business does not exist.');
  }
};

export default OddBusiness;
