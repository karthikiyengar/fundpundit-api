export const Schema = `
    type Error {
        param: String,
        value: String,
        message: String
    }
`;