import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';
import Company from './company';

const mongooseSchema = mongoose.Schema;

const serviceSchema = new mongooseSchema({
  label: {
    type: String,
    required: true,
    unique: true
  },
  icon: {
    type: String
  },
  image: { type: String },
  imagePublicId: { type: String },
}, { timestamps: true });

const Service = mongoose.model('Service', serviceSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type Service {
        id: ID
        label: String
        image: String
        imagePublicId: String
        companies: [Company]
    }
`;

export const Resolvers = {
  Service: {
    id(parent) {
      return parent._id;
    },
    companies: parent => Company.find({ serviceId: parent._id })
  }
};

export const Queries = {
  async allServices() {
    return Service.find({});
  },
  async service(parent, args) {
    return Service.findById(args.id);
  },
};

export const Mutations = {
  async addService(parent, args, context) {
    await Joi.validate(args, paramValidation.addService);
    checkUser(context, ['site-admin']);
    const { label, image } = args;
    const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
    const service = new Service({
      label,
      image: uploaded.url,
      imagePublicId: uploaded.public_id,
    });
    return service.save();
  },
  async updateService(parent, args, context) {
    await Joi.validate(args, paramValidation.updateService);
    checkUser(context, ['site-admin']);
    const { id, label, image } = args;
    const service = await Service.findById(id);
    if (service) {
      service.label = label || service.label;
      if (image) {
        cloudinary.v2.uploader.destroy(service.imagePublicId);
        const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
        service.image = uploaded.url;
        service.imagePublicId = uploaded.public_id;
      }
      return service.save();
    }
    throw Error('Service does not exist.');
  },
  async deleteService(parent, args, context) {
    checkUser(context, ['site-admin']);
    const service = await Service.findById(args.id);
    if (service) {
      await Company.remove({ serviceId: args.id });
      return service.remove();
    }
    throw Error('Service does not exist.');
  }
};

export default Service;
