import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const youtubeLinkSchema = new mongooseSchema({
  link: { type: String, required: true },
  title: { type: String },
  description: { type: String }
}, { timestamps: true });

const YoutubeLink = mongoose.model('YoutubeLink', youtubeLinkSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type YoutubeLink {
        id: ID
        link: String
        title: String
        description: String
    }
`;

export const Queries = {
  async allYoutubeLinks() {
    return YoutubeLink.find();
  },
  async youtubeLink(parent, args) {
    return YoutubeLink.findById(args.id);
  },
};

export const Mutations = {
  async addYoutubeLink(parent, args, context) {
    await Joi.validate(args, paramValidation.addYoutubeLink);
    checkUser(context, ['site-admin']);
    const { link, title, description } = args;
    const youtubeLink = new YoutubeLink({
      link,
      title,
      description
    });
    return youtubeLink.save();
  },
  async updateYoutubeLink(parent, args, context) {
    await Joi.validate(args, paramValidation.updateYoutubeLink);
    checkUser(context, ['site-admin', 'user']);
    const { id, link, title, description } = args;
    const youtubeLink = await YoutubeLink.findById(id);
    if (youtubeLink) {
      youtubeLink.link = link || youtubeLink.link;
      youtubeLink.title = title || youtubeLink.title;
      youtubeLink.description = description || youtubeLink.description;
      return youtubeLink.save();
    }
    throw Error('YoutubeLink does not exist.');
  },
  async deleteYoutubeLink(parent, args, context) {
    checkUser(context, ['site-admin', 'user']);
    const youtubeLink = await YoutubeLink.findById(args.id);
    if (youtubeLink) {
      return youtubeLink.remove();
    }
    throw Error('YoutubeLink does not exist.');
  }
};

export default YoutubeLink;
