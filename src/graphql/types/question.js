import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

// Mongoose Schema
const questionsSchema = new mongoose.Schema({
  question: {
    type: String
  },
  subproductId: {
    type: mongoose.Schema.Types.ObjectId
  },
  weight: {
    type: Number
  },
  hasCalculator: {
    type: Boolean,
    default: false
  },
  options: [{
    _id: mongoose.Schema.Types.ObjectId,
    label: String,
    value: Number
  }]
});

const Questions = mongoose.model('Questions', questionsSchema);

export const Schema = `
  type Question {
    id: ID,
    question: String,
    subproductId: ID,
    weight: Float,
    hasCalculator: Boolean,
    options: [Option]
  }
  type Option {
    id: ID
    label: String
    value: Float
  }
  type Image {
    link: String
  }
  input OptionsArg {
    id: ID
    label: String
    value: String
  }
`;
export const Resolvers = {
  Question: {
    id(parent) {
      return parent._id;
    },
    options(parent) {
      return parent.options;
    }
  },
  Option: {
    id(result) {
      return result._id;
    },
  }
};

export const Queries = {
  async questions(parent, args) {
    return Questions.find({ subproductId: args.subproductId });
  },
  async question(parent, args) {
    return Questions.findById(args.id);
  }
};

export const Mutations = {
  async addQuestion(parent, args, context) {
    await Joi.validate(args, paramValidation.addQuestion);
    checkUser(context, ['site-admin']);
    args.options = args.options.map(item => ({ _id: mongoose.Types.ObjectId(), ...item }));
    const question = new Questions(args);
    return question.save();
  },
  async deleteQuestion(parent, args, context) {
    await Joi.validate(args, paramValidation.deleteQuestion);
    checkUser(context, ['site-admin']);
    return Questions.findByIdAndRemove(args.id);
  }
};

export default Questions;
