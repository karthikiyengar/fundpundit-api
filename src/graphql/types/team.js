import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const teamSchema = new mongooseSchema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  image: {
    type: String,
    required: true
  },
  imagePublicId: { type: String },
  description: {
    type: String,
    required: true
  }
}, { timestamps: true });

const Team = mongoose.model('Team', teamSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type Team {
        id: ID
        name: String
        image: String
        imagePublicId: String
        description: String
    }
`;

export const Resolvers = {
  Team: {
    id(parent) {
      return parent._id;
    }
  }
};

export const Queries = {
  async allTeams() {
    return Team.find({});
  },
  async team(parent, args) {
    return Team.findById(args.id);
  },
};

export const Mutations = {
  async addTeam(parent, args, context) {
    await Joi.validate(args, paramValidation.addTeam);
    checkUser(context, ['site-admin']);
    const { name, image, description } = args;
    const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
    const team = new Team({
      name,
      image: uploaded.url,
      imagePublicId: uploaded.public_id,
      description
    });
    return await team.save();
  },
  async updateTeam(parent, args, context) {
    await Joi.validate(args, paramValidation.updateTeam);
    checkUser(context, ['site-admin']);
    const { id, name, image, description } = args;
    const team = await Team.findById(id);
    if (team) {
      team.name = name || team.name;
      team.description = description || team.description;
      if (image) {
        cloudinary.v2.uploader.destroy(team.imagePublicId);
        const uploaded = await cloudinary.v2.uploader.upload(image, { quality: 30 });
        team.image = uploaded.url;
        team.imagePublicId = uploaded.public_id;
      }
      return team.save();
    }
    throw Error('Team member does not exist.');
  },
  async deleteTeam(parent, args, context) {
    checkUser(context, ['site-admin']);
    const team = await Team.findById(args.id);
    if (team) {
      cloudinary.v2.uploader.destroy(team.imagePublicId);
      return team.remove();
    }
    throw Error('Team member does not exist.');
  }
};

export default Team;
