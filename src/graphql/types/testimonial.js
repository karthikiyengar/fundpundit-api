import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';
import User from './user';

const mongooseSchema = mongoose.Schema;

const testimonialSchema = new mongooseSchema({
  branchId: { type: mongoose.Schema.Types.ObjectId },
  userId: { type: mongoose.Schema.Types.ObjectId, required: true },
  rating: { type: Number },
  review: { type: String },
  isVerified: Boolean
}, { timestamps: true });

const Testimonial = mongoose.model('Testimonial', testimonialSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type Testimonial {
        id: ID
        branchId: ID
        userId: ID
        user: User
        rating: Float
        review: String
        createdAt: String
        isVerified: Boolean
    }
`;

export const Resolvers = {
  Testimonial: {
    id(parent) {
      return parent._id;
    },
    user(parent) {
      return User.findById(parent.userId);
    }
  }
};

export const Queries = {
  async allTestimonials(parent, args) {
    return Testimonial.find(args);
  },
  async testimonial(parent, args) {
    return Testimonial.findById(args.id);
  },
};

export const Mutations = {
  async addTestimonial(parent, args, context) {
    await Joi.validate(args, paramValidation.addTestimonial);
    const { branchId, userId, rating, review } = args;
    const testimonial = new Testimonial({
      branchId,
      userId,
      rating,
      review,
      isVerified: false
    });
    return testimonial.save();
  },
  async updateTestimonial(parent, args, context) {
    await Joi.validate(args, paramValidation.updateTestimonial);
    checkUser(context, ['site-admin']);
    const { id, review, isVerified } = args;
    const testimonial = await Testimonial.findById(id);
    if (testimonial) {
      testimonial.review = review || testimonial.review;
      testimonial.isVerified = isVerified === undefined ? testimonial.isVerified : isVerified;
      return testimonial.save();
    }
    throw Error('Testimonial does not exist.');
  },
  async deleteTestimonial(parent, args, context) {
    checkUser(context, ['site-admin', 'user']);
    const testimonial = await Testimonial.findById(args.id);
    if (testimonial) {
      cloudinary.v2.uploader.destroy(testimonial.imagePublicId);
      return testimonial.remove();
    }
    throw Error('Testimonial does not exist.');
  }
};

export default Testimonial;
