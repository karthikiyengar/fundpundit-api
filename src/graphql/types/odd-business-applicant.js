import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import OddBusiness from './odd-business';
import City from './city';
import SiteCityAdmin from './site-city-admin';
import { sendOddBusinessAppication } from '../../lib/util';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;
const oddBusinessApplicantSchema = new mongooseSchema({
  fullName: { type: String, required: true },
  email: { type: String, required: true },
  amount: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  comment: String,
  oddBusinessId: { type: mongoose.Schema.Types.ObjectId, required: true },
  oddBusinessLabel: String,
  userId: { type: mongoose.Schema.Types.ObjectId },
  cityId: { type: mongoose.Schema.Types.ObjectId }
}, { timestamps: true });

const OddBusinessApplicant = mongoose.model('oddBusinessApplicant', oddBusinessApplicantSchema);

export const Schema = `
    type OddBusinessApplicant {
        id: ID
        fullName: String
        email: String
        amount: String
        phoneNumber: String
        comment: String
        oddBusinessId: ID
        oddBusinessLabel: String
        userId: ID
        cityId: ID
        city: String
    }
`;

export const Resolvers = {
  OddBusinessApplicant: {
    id(parent) {
      return parent._id;
    },
    async city(parent) {
      const city = await City.findById(parent.cityId);
      return city.name;
    }
  }
};

export const Queries = {
  async allOddBusinessApplicants(parent, args, context) {
    checkUser(context, ['site-admin', 'site-city-admin']);
    return OddBusinessApplicant.find({}).sort({ createdAt: -1 });
  }
};

export const Mutations = {
  async addOddBusinessApplicant(parent, args, context) {
    await Joi.validate(args, paramValidation.addOddBusinessApplicant);
    const { fullName, email, amount, phoneNumber, comment, oddBusinessId, city } = args;
    const oddBusinessObject = await OddBusiness.findById(oddBusinessId);
    const oddBusinessApplicant = new OddBusinessApplicant({
      fullName,
      email,
      amount,
      phoneNumber,
      oddBusinessId,
      oddBusinessLabel: oddBusinessObject.label,
      comment,
      cityId: city,
      userId: context.user !== undefined ? context.user.id : null
    });
    const cityObject = await City.findById(city);
    const siteCityAdmins = await SiteCityAdmin.find({ cityId: city });
    const oddBusiness = await OddBusiness.findById(oddBusinessId);
    for (const admin of siteCityAdmins) {
      sendOddBusinessAppication(admin.email, admin.firstName, fullName, cityObject.name, email, phoneNumber, oddBusiness.label, amount);
    }
    return oddBusinessApplicant.save();
  }
};

export default OddBusinessApplicant;
