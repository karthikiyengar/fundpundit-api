import mongoose from 'mongoose';
import { v4 } from 'uuid';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import City from './city';
import Permissions from './permission.js';
import { generateSalt, sha512, checkUser } from '../../lib/auth';
import { userTypes } from '../../lib/constants';
import { sendSiteCityAdminVerificationEmail } from '../../lib/util';

/**
 * Mongoose Schema
 */
const mongooseSchema = mongoose.Schema;
const siteCityAdminSchema = new mongooseSchema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  salt: String,
  password: String,
  cityId: { type: mongooseSchema.Types.ObjectId },
  permissions: Array,
  verificationToken: String,
  isVerified: { type: Boolean, default: false }
}, {
  timestamps: true
});

const SiteCityAdmin = mongoose.model('SiteCityAdmin', siteCityAdminSchema);

/**
 * GraphQL Types
 */
export const Schema = `
  type SiteCityAdmin {
    id: ID
    firstName: String
    lastName: String
    fullName: String
    email: String
    cityId: ID
    bankId: ID
    city: City
    isVerified: Boolean
    verificationToken: String
  }
`;

export const Resolvers = {
  SiteCityAdmin: {
    id: parent => parent._id,
    city: parent => City.findById(parent.cityId),
    fullName: parent => `${parent.firstName} ${parent.lastName}`
  }
};

export const Queries = {
  siteCityAdmin(parent, args, context) {
    if (context.user && context.type === userTypes.SITE_CITY_ADMIN) {
      return context.user;
    }
    return null;
  },
  siteCityAdmins(_, { bankId }, context) {
    checkUser(context, ['site-admin']);
    return SiteCityAdmin.find({});
  }
};

export const Mutations = {
  async addSiteCityAdmin(parent, args, context) {
    await Joi.validate(args, paramValidation.addSiteCityAdmin);
    const permissions = await Permissions.findOne({ type: userTypes.CITY_ADMIN });
    checkUser(context, ['site-admin']);
    const { firstName, lastName, email, cityId, password } = args;
    const verificationToken = v4();
    const payload = {
      firstName,
      lastName,
      email,
      cityId,
      verificationToken,
      permissions: permissions ? permissions.roles : null
    };
    if (password) {
      const salt = generateSalt();
      payload.salt = salt;
      payload.password = sha512(password, salt);
    }
    const siteCityAdmin = new SiteCityAdmin(payload);
    const siteCityAdminObject = await siteCityAdmin.save();
    sendSiteCityAdminVerificationEmail(email, firstName, siteCityAdminObject.toJSON()._id, verificationToken);
    return siteCityAdminObject;
  },
  async deleteSiteCityAdmin(_, { id }, context) {
    checkUser(context, ['site-admin']);
    const siteCityAdmin = await SiteCityAdmin.findById(id);
    if (siteCityAdmin) {
      return siteCityAdmin.remove();
    }
    throw Error('Site city admin does not exist.');
  }
};

export default SiteCityAdmin;
