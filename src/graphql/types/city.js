import mongoose from 'mongoose';
import Branch from './branch';
import { userTypes } from './../../lib/constants';


const mongooseSchema = mongoose.Schema;
const citySchema = new mongooseSchema({
  name: { type: String, required: true, unique: true },
  image: { type: String }
}, { timestamps: true });

const City = mongoose.model('City', citySchema);

export const Schema = `
    type City {
        id: ID
        name: String!
        image: String
        branchCount: Int
    }
`;

export const Resolvers = {
  City: {
    id: parent => parent._id.toString(),
    branchCount: async ({ id }, args, context) => {
      if (context.bankId) {
        return Branch.find({ cityId: id, bankId: context.bankId }).count();
      }
      if (context.type === userTypes.BANK_ADMIN || context.type === userTypes.CITY_ADMIN) {
        return Branch.find({ cityId: id, bankId: context.user.bankId }).count();
      }
      return null;
    }
  }
};

export const Queries = {
  async allCities(parent, args, context) {
    if (args.bankId) {
      context.bankId = args.bankId; // for branch-count resolver
    }
    return City.find({});
  }
};


export default City;
