import { makeExecutableSchema } from 'graphql-tools';
import * as bank from './bank';
import * as branch from './branch';
import * as auth from './auth';
import * as city from './city';
import * as bankAdmin from './bank-admin';
import * as cityAdmin from './city-admin';
import * as siteCityAdmin from './site-city-admin';
import * as subproduct from './subproduct';
import * as questions from './question';
import * as kyr from './kyr';
import * as user from './user';
import * as service from './service';
import * as company from './company';
import * as team from './team';
import * as testimonial from './testimonial';
import * as oddBusiness from './odd-business';
import * as oddBusinessApplicant from './odd-business-applicant';
import * as fundGyan from './fund-gyan';
import * as popupUser from './popup-user';
import * as youtubeLink from './youtube-link';
import * as offer from './offer';
import { Schema as errorSchema } from './error';

const Query = `
    type Query {
        # banks
        allBanks: [Bank]
        bank(id: ID, cityId: ID): Bank
        allBranches: [Branch]
        branch(id: ID): Branch
        allSearchResults(subproduct: ID!, amount: ID!, sector: ID, stage: ID, city: ID!): [Bank]
        suggestOtherBranches(subproduct: ID, city: ID, branch: ID, kyrId: ID, amount: ID, sector: ID, stage: ID): [Branch]
        # cities
        allCities(bankId: ID): [City]
        bankAdmin: BankAdmin
        bankAdmins(bankId: ID!): [BankAdmin]
        cityAdmins(bankId: ID): [CityAdmin]
        siteCityAdmin: SiteCityAdmin
        siteCityAdmins: [SiteCityAdmin]
        # subproducts
        allSubproducts: [Subproduct]
        subproduct(id: ID!): Subproduct
        # questions
        questions(subproductId: ID!) : [Question]
        question(id: ID!): [Question]
        # user
        user(id: ID): User
        allUsers: [User]
        cityAdmin: CityAdmin
        # auth
        checkTokenValidity(token: String!, user: String!, type: String!): CheckTokenResponse
        # kyr
        kyr(id: ID): Kyr
        allKyrsForCity(cityId: ID, bankId: ID, filter: String): [Kyr]
        cityKyrSummary(cityId: ID, bankId: ID): [CitySummary]
        # service
        allServices: [Service]
        service(id: ID!): Service
        # company
        allCompanies: [Company]
        company(id: ID!): Company
        # team
        allTeams: [Team]
        team(id: ID!): Team
        # testimonial
        allTestimonials(isVerified: Boolean): [Testimonial]
        testimonial(id: ID!): Testimonial
        # oddBusiness
        allOddBusinesses: [OddBusiness]
        oddBusiness(id: ID!): OddBusiness
        allOddBusinessApplicants: [OddBusinessApplicant]
        # fundGyan
        allFundGyans: [FundGyan]
        fundGyan(id: ID!): FundGyan
        # youtubeLink
        allYoutubeLinks: [YoutubeLink]
        youtubeLink(id: ID!): YoutubeLink
        # offer
        allOffers: [Offer]
        offer(id: ID!): Offer
        # popupUser
        allPopupUsers: [PopupUser]
    }
`;

const Mutations = `
    type Mutation {
        # banks
        addBank (name: String!, email: String, contactNumber: String!, address: String!, logo: String!, description: String!, subproduct: ID!, details: [BankDetailsArg], criteria: CriteriaArg): Bank
        updateBank (id: ID!, name: String, email: String, contactNumber: String, address: String, logo: String, description: String, subproducts: SubproductArg, details: [BankDetailsArg], criteria: CriteriaArg): Bank
        deleteBank (id: ID!) : Bank
        # branch
        addBranch (address: String!, location: String!, branchEmail: String, bankId: ID, cityId: ID!, latitude: String, longitude: String, contactNumber: String, email: String): Branch
        updateBranch (id: ID!, address: String, location: String, branchEmail: String, cityId: ID, latitude: String, longitude: String, contactNumber: String, email: String) : Branch
        deleteBranch (id: ID!) : Branch
        # bankAdmin
        addBankAdmin (firstName: String, lastName: String, email: String!, bankId: ID!): BankAdmin
        updateBankAdmin (id: ID!, firstName: String, lastName: String, email: String! bankId: ID) : BankAdmin
        deleteBankAdmin (id: ID!) : BankAdmin
        # cityAdmin
        addCityAdmin (firstName: String, lastName: String, email: String!, cityId: ID!, password: String, bankId: ID): CityAdmin
        updateCityAdmin (id: ID!, firstName: String, lastName: String) : CityAdmin
        deleteCityAdmin (id: ID!) : CityAdmin
        # siteCityAdmin
        addSiteCityAdmin (firstName: String, lastName: String, email: String!, cityId: ID!, password: String): CityAdmin
        deleteSiteCityAdmin (id: ID!) : CityAdmin
        # users
        addUser (firstName: String!, lastName: String!, email: String!, password: String!) : User
        userRegister (firstName: String!, lastName: String!, email: String!, phoneNumber: String!, password: String!) : User
        updateUser (firstName: String, lastName: String, profilePic: String) : User
        # auth
        login (type: String!, email: String!, password: String!): User
        setPassword (password: String, confirmPassword: String, user: String, token: String, type: String) : User
        updatePassword (currentPassword: String!, password: String!, confirmPassword: String!) : User
        forgotPassword (email: String!, type: String!): User
        # questions
        submitKyr (subproductId: ID!, branchId: ID!, amountId: ID!, sectorId: ID, stageId: ID, responses: [ResponseArg]!, user: NewUserArg, otherBranches: [ID]) : [Kyr]
        setKyrFlag (kyrId: ID!, flag: String!, value: Boolean!, file: String, reason: String) : Kyr
        transferKyr(kyrId: ID!, otherBranches: [ID]!): [Kyr]
        uploadUserDocument (id: ID!, file: String) : Kyr
        escalateToAdmin (kyrId: ID!) : Kyr
        # service
        addService (label: String!, image: String!) : Service
        updateService ( id: ID!, label: String, image: String) : Service
        deleteService (id: ID!) : Service
        # company
        addCompany (companyName: String!, contactName: String!, contactNumber: String!, address: String!, description: String!, serviceOffered: String!, serviceId: ID!, companyLogo: String): Company
        updateCompany (id: ID!, companyName: String, contactName: String, contactNumber: String, address: String, description: String, serviceOffered: String!, serviceId: ID, companyLogo: String) : Company
        deleteCompany (id: ID!) : Company
        # team
        addTeam (name: String!, image: String!, description: String!) : Team
        updateTeam (id: ID!, name: String!, image: String, description: String) : Team
        deleteTeam (id: ID!) : Team
        # testimonial
        addTestimonial (branchId: ID!, userId: ID!, rating: String!, review: String!) : Testimonial
        updateTestimonial (id: ID!, review: String, isVerified: Boolean) : Testimonial
        deleteTestimonial (id: ID!) : Testimonial
        # oddBusiness
        addOddBusiness (label: String!, image: String!) : OddBusiness
        updateOddBusiness (id: ID!, label: String!, image: String) : OddBusiness
        deleteOddBusiness (id: ID!) : OddBusiness
        # oddBusinessApplicant
        addOddBusinessApplicant (fullName: String!, email: String!, amount: String!, phoneNumber: String!, comment: String, oddBusinessId: ID!, city: ID) : OddBusinessApplicant
        # question
        deleteQuestion (id: ID!) : Question
        addQuestion (subproductId: ID!, question: String!, weight: String!, options: [OptionsArg], hasCalculator: Boolean) : Question
        # popupUser
        addPopupUser (fullName: String!, email: String!, phoneNumber: String!, subproductId: ID!, amountId: ID!, stageId: ID, sectorId: ID, cityId: ID) : PopupUser
        # fundGyan
        addFundGyan (label: String!, image: String!) : FundGyan
        updateFundGyan ( id: ID!, label: String, image: String!) : FundGyan
        deleteFundGyan (id: ID!) : FundGyan
        # youtubeLink
        addYoutubeLink (link: String!, title: String, description: String) : YoutubeLink
        updateYoutubeLink (id: ID!, link: String, title: String, description: String) : YoutubeLink
        deleteYoutubeLink (id: ID!) : YoutubeLink
        # offer
        addOffer (title: String!, description: String!) : Offer
        updateOffer (id: ID!, title: String, description: String) : Offer
        deleteOffer (id: ID!) : Offer
    }
`;

const rootSchema = `
    schema {
        query: Query
        mutation: Mutation
    }
`;

const queryResolvers = {
  Query: {
    ...bank.Queries,
    ...branch.Queries,
    ...city.Queries,
    ...bankAdmin.Queries,
    ...cityAdmin.Queries,
    ...subproduct.Queries,
    ...questions.Queries,
    ...user.Queries,
    ...kyr.Queries,
    ...service.Queries,
    ...company.Queries,
    ...team.Queries,
    ...testimonial.Queries,
    ...oddBusiness.Queries,
    ...oddBusinessApplicant.Queries,
    ...popupUser.Queries,
    ...fundGyan.Queries,
    ...youtubeLink.Queries,
    ...offer.Queries,
    ...siteCityAdmin.Queries,
    ...auth.Queries
  },
  Mutation: {
    ...bank.Mutations,
    ...branch.Mutations,
    ...bankAdmin.Mutations,
    ...cityAdmin.Mutations,
    ...subproduct.Mutations,
    ...questions.Mutations,
    ...user.Mutations,
    ...kyr.Mutations,
    ...service.Mutations,
    ...company.Mutations,
    ...team.Mutations,
    ...testimonial.Mutations,
    ...oddBusiness.Mutations,
    ...oddBusinessApplicant.Mutations,
    ...popupUser.Mutations,
    ...fundGyan.Mutations,
    ...youtubeLink.Mutations,
    ...offer.Mutations,
    ...siteCityAdmin.Mutations,
    ...auth.Mutations
  }
};


const Schemas = [
  rootSchema,
  Query,
  Mutations,
  errorSchema,
  city.Schema,
  bank.Schema,
  branch.Schema,
  bankAdmin.Schema,
  cityAdmin.Schema,
  subproduct.Schema,
  questions.Schema,
  kyr.Schema,
  user.Schema,
  service.Schema,
  company.Schema,
  team.Schema,
  testimonial.Schema,
  oddBusiness.Schema,
  oddBusinessApplicant.Schema,
  popupUser.Schema,
  fundGyan.Schema,
  youtubeLink.Schema,
  offer.Schema,
  siteCityAdmin.Schema,
  auth.Schema
];

const Resolvers = Object.assign(
  {},
  queryResolvers,
  city.Resolvers,
  bank.Resolvers,
  branch.Resolvers,
  bankAdmin.Resolvers,
  cityAdmin.Resolvers,
  subproduct.Resolvers,
  questions.Resolvers,
  user.Resolvers,
  kyr.Resolvers,
  service.Resolvers,
  company.Resolvers,
  team.Resolvers,
  testimonial.Resolvers,
  oddBusiness.Resolvers,
  oddBusinessApplicant.Resolvers,
  popupUser.Resolvers,
  fundGyan.Resolvers,
  youtubeLink.Resolvers,
  offer.Resolvers,
  siteCityAdmin.Resolvers,
  auth.Resolvers
);

export default makeExecutableSchema({
  typeDefs: Schemas,
  resolvers: Resolvers,
});
