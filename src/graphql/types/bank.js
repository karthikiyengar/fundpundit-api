import mongoose from 'mongoose';
import { v4 } from 'uuid';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import paramValidation from './../../lib/param-validation';
import { userTypes } from './../../lib/constants';
import Branch from './branch';
import BankAdmin from './bank-admin';
import City from './city';
import Subproduct from './subproduct';
import Testimonial from './testimonial';
import { sendBankAdminVerificationEmail } from '../../lib/util';
import { checkUser } from '../../lib/auth';

/**
 * Mongoose Model
 */
const mongooseSchema = mongoose.Schema;
const bankSchema = new mongooseSchema({
  name: { type: String, required: true },
  products: Array,
  image: { type: String },
  contactNumber: {
    type: String,
    required: true
  },
  email: String,
  address: {
    type: String,
    required: true
  },
  description: {
    type: String,
  },
  rating: {
    type: String
  },
  logo: { type: String },
  logoPublicId: { type: String },
  details: Array,
  criteria: {
    amount: [mongoose.Schema.Types.ObjectId],
    sector: [mongoose.Schema.Types.ObjectId],
    stage: [mongoose.Schema.Types.ObjectId],
  },
  subproduct: mongoose.Schema.Types.ObjectId
}, {
  timestamps: true
});

bankSchema.post('remove', async function removeRelatedBranches(removed) {
  const { id } = removed.toJSON();
  await Branch.remove({ bankId: id });
});

const Bank = mongoose.model('Bank', bankSchema);

/**
 * GraphQL Types
 */
export const Schema = `
    type Bank {
      id: ID
      name: String
      branchCount: Int
      branches: [Branch]
      bankAdmins: [BankAdmin]
      cities: [City]
      isVerified: Boolean
      contactNumber: String
      address: String
      logo: String
      logoPublicId: String
      rating: String
      description: String
      verificationToken: String
      details: [BankDetails]
      subproduct: Subproduct
      criteria: Criteria
      reviewCount: Int
      rating: Float
    }
    type Criteria {
      amount: [ID],
      stage: [ID],
      sector: [ID]
    }
    type BankDetails {
      id: ID
      key: String
      label: String
      value: String
    }
    input BankDetailsArg {
      key: String
      value: String
    }
    input SubproductArg {
      id: [ID]
    }
    input CriteriaArg {
      amount: [ID],
      sector: [ID],
      stage: [ID]
    }
`;

export const Resolvers = {
  Bank: {
    id(parent) {
      return parent._id.toString();
    },
    details(parent) {
      return parent.details;
    },
    criteria(parent) {
      return parent.criteria;
    },
    branches(parent, args, context) {
      const query = {
        bankId: parent.id
      };
      // check if city id is passed in context (for search results)
      if (context.cityId) {
        query.cityId = context.cityId;
      }
      return Branch.find(query);
    },
    bankAdmins(parent, args, context) {
      if (context.type == null || context.type === userTypes.USER) return null;
      return BankAdmin.find({ bankId: parent.id });
    },
    branchCount(parent, args, context) {
      const query = {
        bankId: parent.id
      };
      // check if city id is passed in context (for search results)
      if (context.cityId) {
        query.cityId = context.cityId;
      }
      return Branch.count(query);
    },
    subproduct(parent) {
      return Subproduct.findById(parent.subproduct);
    },
    async cities(parent) {
      const cities = await Branch.find({ bankId: parent.id }).distinct('cityId');
      return City.find({ id: { $in: cities } });
    },
    async reviewCount(parent) {
      const branches = await Branch.find({ bankId: parent.id }).select('_id');
      return Testimonial.count({ branchId: { $in: branches.map(item => item._id) } });
    },
    async rating(parent) {
      const branches = await Branch.find({ bankId: parent.id }).select('_id');
      const rating = await Testimonial.aggregate([
        {
          $match: {
            branchId: {
              $in: branches.map(item => item._id)
            }
          }
        },
        {
          $group: {
            _id: null,
            rating: { $avg: '$rating' }
          }
        }
      ]);
      return rating.length > 0 ? rating[0].rating : null;
    }
  }
};

export const Mutations = {
  async addBank(parent, args, context) {
    await Joi.validate(args, paramValidation.addBank);
    checkUser(context, ['site-admin', 'site-city-admin']);
    const { name, email, contactNumber, address, logo, subproduct, details, description, criteria } = args;
    if (email && BankAdmin.find({ email }).count() > 0) {
      throw Error('Email already exists');
    }
    const uploaded = await cloudinary.v2.uploader.upload(logo, { quality: 30 });

    // for dynamic text fields depending on subproduct type
    const subproductObject = await Subproduct.findById(subproduct);
    const enrichedDetails = details ? details.map((item) => {
      item.id = mongoose.Types.ObjectId();
      item.label = subproductObject.details.find(detail => detail.key === item.key).label;
      return item;
    }) : [];

    const verificationToken = v4();
    const bank = new Bank({
      name,
      contactNumber,
      address,
      description,
      email,
      logo: uploaded.url,
      logoPublicId: uploaded.public_id,
      subproduct,
      details: enrichedDetails,
      criteria
    });
    const bankObject = await bank.save();
    const subproductDetails = await Subproduct.findById(subproduct);
    if (email) {
      const admin = new BankAdmin({
        firstName: name,
        lastName: `(${subproductDetails.label})`,
        email,
        bankId: bankObject.toJSON()._id,
        verificationToken
      });
      const adminObject = await admin.save();
      sendBankAdminVerificationEmail(email, name, adminObject.toJSON()._id, verificationToken);
    }
    return bankObject;
  },
  async updateBank(parent, args, context) {
    await Joi.validate(args, paramValidation.updateBank);
    checkUser(context, ['site-admin', 'site-city-admin']);
    const { id, name, contactNumber, address, description, logo, details, email, criteria } = args;
    const bank = await Bank.findById(id);
    if (bank) {
      const subproductObject = await Subproduct.findById(bank.subproduct);
      const enrichedDetails = details ? details.map((item) => {
        item.id = mongoose.Types.ObjectId();
        item.label = subproductObject.details.find(detail => detail.key === item.key).label;
        return item;
      }) : null;

      if (email && email !== bank.email) {
        const verificationToken = v4();
        const existingAdmin = await BankAdmin.findOne({ email });
        let adminObject;
        if (existingAdmin) {
          existingAdmin.email = email;
          adminObject = await existingAdmin.save();
        } else {
          const admin = new BankAdmin({
            firstName: name,
            lastName: `(${subproductObject.label})`,
            email,
            bankId: bank.toJSON()._id,
            verificationToken
          });
          adminObject = await admin.save();
        }
        sendBankAdminVerificationEmail(email, name, adminObject.toJSON()._id, verificationToken);
      }
      bank.name = name || bank.name;
      bank.email = email || bank.email;
      bank.contactNumber = contactNumber || bank.contactNumber;
      bank.address = address || bank.address;
      bank.criteria = criteria || bank.criteria;
      bank.description = description || bank.description;
      bank.details = enrichedDetails || bank.details;
      if (logo) {
        cloudinary.v2.uploader.destroy(bank.logoPublicId);
        const uploaded = await cloudinary.v2.uploader.upload(logo, { quality: 30 });
        bank.logo = uploaded.url;
        bank.logoPublicId = uploaded.public_id;
      }
      return bank.save();
    }
    throw Error('Bank does not exist');
  },
  async deleteBank(parent, args, context) {
    checkUser(context, ['site-admin', 'site-city-admin']);
    const { id } = args;
    const bank = await Bank.findById(id);
    if (bank) {
      return bank.remove();
    }
    throw Error('Bank does not exist.');
  }
};

export const Queries = {
  async allBanks(parent, args, context) {
    checkUser(context, ['site-admin']);
    return Bank.find({});
  },
  async bank(parent, args, context) {
    let { id } = args;
    if (context.type === userTypes.BANK_ADMIN) {
      id = context.user.bankId;
    }
    if (args.cityId) {
      context.cityId = args.cityId; // so that resolvers can know cityId (e.g branches)
    }
    return Bank.findById(id);
  },
  async allSearchResults(parent, args, context) {
    const { subproduct, city, amount, sector, stage } = args;
    // set city id to context for resolvers
    context.cityId = city;

    // Finding bankIds from current city branches
    const banks = await Bank.find({ subproduct });
    const bankIds = await Branch.find({ cityId: city, bankId: { $in: banks } })
      .then(response => response.map(branch => branch.bankId));

    const searchQuery = {};
    if (amount) {
      searchQuery['criteria.amount'] = amount;
    }
    if (sector) {
      searchQuery['criteria.sector'] = sector;
    }
    if (stage) {
      searchQuery['criteria.stage'] = stage;
    }
    return Bank.find({
      subproduct, // where subproduct matches
      _id: { $in: bankIds }, // only for the current city
      ...searchQuery
    });
  }
};

export default Bank;
