
import mongoose from 'mongoose';
import Joi from 'joi';
import paramValidation from './../../lib/param-validation';
import { checkUser } from '../../lib/auth';

const mongooseSchema = mongoose.Schema;

const offerSchema = new mongooseSchema({
  title: { type: String },
  description: { type: String }
}, { timestamps: true });

const Offer = mongoose.model('Offer', offerSchema);

export const Schema = `
    type Offer {
        id: ID
        title: String
        description: String
    }
`;

export const Resolvers = {
  Offer: {
    id(parent) {
      return parent._id.toString();
    }
  }
};

export const Queries = {
  async allOffers() {
    return Offer.find({});
  },
  async offer(parent, args) {
    const { id } = args;
    const offer = Offer.findById(id);
    if (offer) {
      return offer;
    }
    throw Error('Offer does not exist.');
  }
};

export const Mutations = {
  async addOffer(parent, args, context) {
    checkUser(context, ['site-admin']);
    await Joi.validate(args, paramValidation.addOffer);
    const { title, description } = args;
    const offer = new Offer({
      title,
      description
    });
    return offer.save();
  },
  async updateOffer(parent, args, context) {
    await Joi.validate(args, paramValidation.updateOffer);
    checkUser(context, ['site-admin']);
    const { id, title, description } = args;
    const offer = await Offer.findById(id);
    if (offer) {
      offer.title = title || offer.title;
      offer.description = description || offer.description;
      return offer.save();
    }
    throw Error('Offer does not exist.');
  },
  async deleteOffer(parent, args, context) {
    checkUser(context, ['site-admin']);
    const { id } = args;
    const offer = await Offer.findById(id);
    if (offer) {
      return offer.remove();
    }
    throw Error('Offer does not exist.');
  }
};

export default Offer;
