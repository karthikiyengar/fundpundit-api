import mongoose from 'mongoose';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import { v4 } from 'uuid';
import { generateSalt, sha512, checkUser } from '../../lib/auth';
import { userTypes } from '../../lib/constants';
import paramValidation from '../../lib/param-validation';
import Permissions from './permission';
import Kyr from './kyr';
import { sendVerificationEmail } from '../../lib/util';

// Mongoose Schema

const mongooseSchema = mongoose.Schema;
const userSchema = new mongooseSchema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  profilePic: {
    type: String
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String
  },
  salt: {
    type: String
  },
  permissions: {
    type: Array
  },
  phoneNumber: {
    type: String
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  verificationToken: {
    type: String
  },
  forgotPassword: Boolean
}, {
  timestamps: true
});

const User = mongoose.model('User', userSchema);

// GraphQL Types

export const Schema = `
  type LoginResponse {
    token: String
    user: User
  }
  type User {
    id: ID
    firstName: String
    lastName: String
    fullName: String
    profilePic: String
    email: String
    phoneNumber: String
    token: String
    kyrs: [Kyr]
    type: String
    createdAt: String
    updatedAt: String
  }
  type CheckTokenResponse {
    valid: Boolean
    isPasswordSet: Boolean
    isForgotPassword: Boolean
  }
`;

export const Queries = {
  user(parent, args, context) {
    if (args.id) {
      return User.findById(args.id);
    }
    return context.user;
  },
  allUsers(parent, args, context) {
    checkUser(context, ['site-admin', 'site-city-admin']);
    return User.find({}).sort({ createdAt: -1 });
  },
};

export async function createUser(args) {
  const { firstName, lastName, phoneNumber, email, password } = args;
  const permissions = await Permissions.findOne({ type: userTypes.USER });
  const verificationToken = v4();
  const payload = {
    firstName,
    lastName,
    email,
    phoneNumber,
    verificationToken,
    permissions: permissions.roles
  };
  if (password) {
    const salt = generateSalt();
    payload.salt = salt;
    payload.password = sha512(password, salt);
  }
  const user = new User(payload);
  const userObject = await user.save();
  sendVerificationEmail(email, firstName, lastName, userObject.toJSON()._id, verificationToken);
  return userObject;
}

export const Mutations = {
  async addUser(parent, args) {
    await Joi.validate(args, paramValidation.addUser);
    return createUser(args);
  },
  async updateUser(parent, args, context) {
    await Joi.validate(args, paramValidation.updateUser);
    checkUser(context, ['site-admin', 'user']);
    const { firstName, lastName, profilePic } = args;
    const user = await User.findById(context.user.id);
    if (user) {
      user.firstName = firstName || user.firstName;
      user.lastName = lastName || user.lastName;
      if (profilePic) {
        cloudinary.v2.uploader.destroy(user.profilePicPublicId);
        const uploaded = await cloudinary.v2.uploader.upload(profilePic, { quality: 30 });
        user.profilePic = uploaded.url;
        user.profilePicPublicId = uploaded.public_id;
      }
      return user.save();
    }
    throw Error('User not found, Please try again.');
  },
  async userRegister(parent, args) {
    await Joi.validate(args, paramValidation.registerUser);
    const { firstName, lastName, email, phoneNumber, password } = args;
    const permissions = await Permissions.findOne({ type: userTypes.USER });
    const verificationToken = v4();
    const salt = generateSalt();
    const payload = {
      firstName,
      lastName,
      email,
      phoneNumber,
      password: sha512(password, salt),
      salt,
      verificationToken,
      permissions: permissions.roles
    };
    const user = new User(payload);
    const userObject = await user.save();
    sendVerificationEmail(email, firstName, lastName, userObject.toJSON()._id, verificationToken);
    return userObject;
  }
};
export const Resolvers = {
  User: {
    id(parent) {
      return parent._id;
    },
    kyrs(parent) {
      return Kyr.find({ userId: parent._id }).sort({ createdAt: -1 });
    },
    fullName(parent) {
      return `${parent.firstName} ${parent.lastName}`;
    },
    type(parent, args, context) {
      return context.type;
    }
  },
  LoginResponse: {
    user(parent) {
      return parent.user;
    }
  }
};

export default User;
