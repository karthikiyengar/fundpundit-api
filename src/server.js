// @flow
import fs from 'fs';
import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';
import graphqlHTTP from 'express-graphql';
import bunyanLogger from 'express-bunyan-logger2';
import cors from 'cors';
import bluebird from 'bluebird';
import Joi from 'joi';
import cloudinary from 'cloudinary';
import logger from './lib/logger';
import formatter from './middleware/formatter';
import checkToken from './middleware/jwt';
import schema from './graphql/types/root.js';

// create logs directory if does not exist
if (!fs.existsSync('logs')) {
  fs.mkdirSync('logs');
}

// use bluebird as default promise library
global.Promise = bluebird;
mongoose.Promise = bluebird;

// add global mongoose middleware
dotenv.config();

// promisify Joi
Joi.validate = bluebird.promisify(Joi.validate);

// set up cloudinary cloudinaryCredentials
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});

const app = express();
const port = process.env.PORT || 3000;

try {
  mongoose.connect(process.env.DB_HOST);
} catch (err) {
  logger.error(err);
}
app.use(cors());
app.use(formatter);
app.use(bodyParser.json({ limit: '5mb' }));
app.use(expressValidator());

app.use(bunyanLogger({
  name: 'audit',
  streams: [{
    path: './logs/audit.log',
  }, {
    stream: process.stdout
  }],
  color: true,
  includesFn(req) {
    if (req.user) {
      return {
        user: req.user
      };
    }
    return {
      user: null
    };
  }
}));

app.use(bunyanLogger.errorLogger({
  name: 'error',
  streams: [{
    path: './logs/error.log',
  }, {
    stream: process.stdout
  }],
  color: true,
  includesFn(req) {
    if (req.user) {
      return {
        user: req.user
      };
    }
    return {
      user: null
    };
  }
}));

app.use('/graphql', checkToken, graphqlHTTP({
  schema,
  graphiql: process.env.NODE_ENV !== 'production'
}));

app.listen(port, '0.0.0.0', () => {
  logger.info(`Server Listening at ${port}`);
});
