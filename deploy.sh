rm -rf dist;
npm run build;
cp package.json dist
cp .env dist
rsync -azr --progress --delete dist/ --exclude node_modules fundpundit@108.170.54.236:~/fundpundit-api
ssh fundpundit@108.170.54.236 "cd ~/fundpundit-api; yarn install";
ssh fundpundit@108.170.54.236 "pm2 restart graphql"
