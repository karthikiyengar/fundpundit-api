const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: './src/server.js',
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
  devtool: 'sourcemap',
  plugins: [
    new webpack.BannerPlugin('require("source-map-support").install();',
                           { raw: true, entryOnly: false }),
    new CopyWebpackPlugin([{
      from: './src/templates/',
      to: './templates'
    }])
  ],
  output: {
    filename: 'bundle.js',
    path: __dirname + '/dist'
  },
  module: {
    loaders: [{
      test: /.js/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'stage-2'],
        plugins: ['transform-flow-strip-types', 'transform-runtime']
      }
    }]
  }
};
